'use strict';

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    uglify = require('gulp-uglify'),
    jshint = require('gulp-jshint'),
    rigger = require('gulp-rigger'),
    minifyCSS = require('gulp-minify-css');

var config = {
    paths: {
        build: {
            // Django 'static' mounts on root in Docker
            js: '/static/js/',
            styles: '/static/css/'
        },
        src: {
            js: 'src/js/main.js',
            styles: 'src/sass/styles.scss'
        },
        watch: {
            js: 'src/js/**/*.js',
            styles: 'src/sass/**/*.scss'
        }
    }
};

gulp.task('styles:build', function () {
    gulp.src(config.paths.src.styles)
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(minifyCSS())
        .pipe(gulp.dest(config.paths.build.styles));
});

gulp.task('js:build', function () {
    gulp.src(config.paths.src.js)
        .pipe(jshint())
        .pipe(jshint.reporter('default'))
        .pipe(rigger())
        .pipe(uglify())
        .pipe(gulp.dest(config.paths.build.js))
});

gulp.task('watch', function () {
    gulp.watch([config.paths.watch.styles], function () {
        gulp.start('styles:build');
    });
    gulp.watch(config.paths.watch.js, function () {
        gulp.start('js:build');
    })
});

gulp.task('build', ['styles:build', 'js:build']);
gulp.task('default', ['styles:build', 'js:build', 'watch']);