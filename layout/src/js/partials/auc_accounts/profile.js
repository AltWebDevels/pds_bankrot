/**
 * Created by ideath on 06.12.15.
 */
var sendSmsLink = $('.auc_accounts__profile .send_sms'),
    userId = sendSmsLink.data().user,
    sendForm = $('.auc_accounts__profile .send-form');

sendSmsLink.click(function () {
    $.ajax({
        url: "/auc_accounts/send_sms_ajax/" + userId
    }).done(function (res) {
        if (res.success) {
            sendSmsLink.toggle();
            sendForm.toggle();
            alert('Введить код с СМС в поле');
        } else {
            alert('Ошибка отправки СМС');
        }

    });
});