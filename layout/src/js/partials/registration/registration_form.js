// accept checkbox
var check_me_question = $('.question_form').find('.check_me');
var check_btn_question = $('.question_form').find('.btn-success');
var check_me_registration = $('.registration_form').find('.check_me');
var check_btn_registration = $('.registration_form').find('.btn-success');
function check(check_me, check_btn) {
    if ($(check_me).prop('checked')) {
        $(check_btn).removeClass('disabled');
        $(check_me).on('click', function () {
            check_btn.toggleClass('disabled');
        });
    }
    else {
        $(check_me).on('click', function () {
            check_btn.toggleClass('disabled');
        });
    }
}

$(function () {
    'use strict';
    check(check_me_question, check_btn_question);
    check(check_me_registration, check_btn_registration);
});

// toggle organization
var ogrnLine = $('.registration__registration_form #id_ogrn').parent().parent();
var lastNameRow = $('.registration__registration_form #id_last_name').parent().parent();
var isOrgCheckbox = $('.registration__registration_form #id_organization');
var firstNameLabel = $('.registration__registration_form label[for="id_first_name"]');


if (!isOrgCheckbox.is(':checked')) {
    ogrnLine.hide();
} else {
    firstNameLabel.html('Название:');
    lastNameRow.hide();
}

isOrgCheckbox.click(function () {
    lastNameRow.toggle();
    ogrnLine.toggle();
    firstNameLabel.html(this.checked ? 'Название:' : 'Имя:');
});