function cl(x) {
    console.log(x);
}
(function ($) {
    $.fn.equalize = function (options) {
        var defaults = {
            columns: '.equel-col',
            resize: true,
            wrapper: '.equel-wrapper',
            min_width: 768
        };

        var settings = $.extend({}, defaults, options);

        var heights = [];

        return this.each(function () {
            var wrapper = $(this);
            equel(wrapper);
            if (settings.resize) {
                $(window).resize(function () {
                    equel(wrapper);
                });
            }
        });

        function equel(wrapper) {
            if ($(window).width() < settings.min_width) {
                wrapper.find(settings.columns).removeAttr('style');
                return false;
            }
            var max_height = 0;
            wrapper.find(settings.columns)
                .removeAttr('style')
                .each(function () {
                    var height = $(this).height();
                    if (height > max_height) {
                        max_height = height;
                    }
                }).height(max_height);
        }
    };


}(jQuery));

function footerBottom() {
    var footer = $('#footer');
    var wrapper = $('.wrapper');
    var browserHeight = $(window).height(),
        footerOuterHeight = footer.outerHeight(true),
        mainHeightMarginPaddingBorder = wrapper.outerHeight(true) - wrapper.height();
    wrapper.css({
        'min-height': browserHeight - footerOuterHeight - mainHeightMarginPaddingBorder,
    });
}


jQuery(document).ready(function () {
    footerBottom();
    $('.wrapper').resize(footerBottom);
    $(window).resize(footerBottom);
    $('.equel-wrapper').equalize();
    jQuery("i.fa-arrows-alt").on("click", function () {
        jQuery(this).parents('.panel').find('.expanded-info').slideToggle();
    });

    jQuery("p.expanded-search").on("click", function () {
        jQuery('.expanded-search-form').slideToggle();
    });

    $('[data-toggle="offcanvas"]').click(function () {
        $('.row-offcanvas').toggleClass('active');
    });

    auctionList.init();
    lot.init();
    authRestriction();

    $('#id_places').multiselect({
        buttonText: function (options, select) {
            return 'Выберите площадку';
        },
        buttonTitle: function (options, select) {
            var labels = [];
            options.each(function () {
                labels.push($(this).text());
            });
            return labels.join(' - ');
        },
        includeSelectAllOption: true,
        maxHeight: 200

    });
    $('#sandbox-container .input-group.date').datepicker();
    cl(1);

    $('.sidebar-nav__item').mouseenter(function(){ 
        $(".sidebar-nav__item").find("p").css("visibility", "visible");
    
    });
    $('.sidebar-nav__item').mouseleave(function(){ 
        $(".sidebar-nav__item").find("p").css("visibility", "hidden");
    
    });
});

// Custom functions, connected with BS
var _bs = {
    alert: function (text, type) {
        var alert = $('<div />', {
            'class': 'alert alert-' + type,
            role: 'alert'
        });
        return alert;
    }
};

var auctionList = {
    options: {
        urls: {
            remove: '',
            fave: '/auction/add_to_chapter/',
            removeFave: '/auction/remove_from_chapter/',
            comment: '',
            addFaveItem: '',
            removeFaveItem: ''
        }
    },
    init: function () {
        this.items = $('.js-auction-1');
        if (!this.items.length && !$('#lot-fave__modal-add').length) {
            return false;
        }
        this.actionButtons();
        this.priceHistory();
        this.addFaveItemUser();
    },
    addFaveItemUser: function () {
        $('.lot-fave__btn--add').click(function () {
            $('#lot-fave__modal-add').modal();
            return false;
        });
    },
    priceHistory: function () {
        $('.auction-1__price_wrap--down').click(function () {
            $(this).find('.auction-1__price-list').toggle();
        });
    },
    actionButtons: function () {
        var self = this;
        self.items.each(function () {
            self.itemId = $(this).data('auctionId');

            if (typeof self.itemId == 'undefined' || !self.itemId) {
                return false;
            }

            self.removeItem($(this));
            self.faveItem($(this));
            self.removeFaveItem($(this));
            self.addComment($(this));
        });
    },
    removeItem: function (listItem) {
        var self = this;
        listItem.find('.js-auction-1-btn-remove').click(function () {
            var data = {
                auctionId: self.itemId
            };
            var url = $(this).data('href');
            $.get(url)
                .done(function (responce) {
                    listItem.remove();
                })
                .fail(function (responce) {

                });
        });
    },
    faveItem: function (listItem) {
        var self = this;
        listItem.find('.auction-1__add-fave').click(function () {
            var thisBtn = $(this);
            var data = {
                auctionId: listItem.data('auctionId'),
                categoryId: $(this).data('categoryId')
            };

            $.get(self.options.urls.fave, data)
                .done(function (responce) {
                    location.reload();
                })
                .fail(function (responce) {

                });
            return false;
        });
    },
    removeFaveItem: function (listItem) {
        var self = this;
        listItem.find('.js-auction-1__remove-fave').click(function () {
            var thisBtn = $(this);
            var data = {
                auctionId: listItem.data('auctionId')
            };
            $.get(self.options.urls.removeFave, data)
                .done(function (responce) {
                    location.reload();
                })
                .fail(function (responce) {

                });
            return false;
        });
    },
    addComment: function (listItem) {
        var self = this;
        var commentModal = listItem.find('.commentModal');
        var commentInput = listItem.find('.comment-text-modal');
        var trigger = listItem.find('.js-auction-1__add-comment');

        trigger.click(function () {
            commentModal.modal();
        });


        var formComment = commentModal.find('.auction-s__form_comment');
        formComment.submit(function () {
            $.post(trigger.data('setUrl'), formComment.serialize())
                .done(function (responce) {
                    cl(responce);
                })
                .fail(function (responce) {

                });
            commentModal.modal('hide');
            return false;
        });


    }
};

var lot = {
    options: {
        url: {
            saveNote: ''
        }
    },
    init: function () {
        var lot = $('.lot');
        if (!lot.length) {
            return false;
        }
        this.lot = lot;
        this.lotId = lot.data('auctionId');
        this.note();
    },
    note: function () {
        var self = this;
        var note = self.lot.find('.lot__note');
        var url = note.data('setUrl');
        var form = note.parents('#auction-s__form_comment');
        note.focus(function () {
            $(this)
                .parents('.lot__note-row')
                .addClass('lot__note-row--active');
        });
        note.blur(function () {
            $(this)
                .parents('.lot__note-row')
                .removeClass('lot__note-row--active');
            var noteVal = $(this).val();
            saveNote(noteVal);
        });

        function saveNote(noteVal) {
            $.post(url, form.serialize())
                .done(function (responce) {
                    cl(responce);
                })
                .fail(function (responce) {

                });
        }
    }
};

function authRestriction() {
    $('.not-logged-in .need-auth')
        .data('toggle', 'tooltip')
        .data('placement', 'top')
        .attr('title', 'Больше только с премиум')
        .tooltip();
}


