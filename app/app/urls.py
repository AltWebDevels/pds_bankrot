from django.conf import settings
from django.conf.urls import include, url, patterns
from django.views.generic import TemplateView
from django.conf.urls.static import static
from django.contrib import admin

urlpatterns = patterns('',
    url(r'^$', TemplateView.as_view(template_name='static_pages/index.html'), name='index'),
    url(r'^auction/', include('auction.urls', namespace='auction')),
    url(r'^accounts/', include('registration.backends.default.urls')),
    url(r'^auc_accounts/', include('auc_accounts.urls', namespace='auc_accounts')),
    url(r'^forums/', include('forums.urls', namespace='forums')),
    url(r'^admin/', include(admin.site.urls)),
    # static pages
    url(r'^about/$', TemplateView.as_view(template_name='static_pages/about.html'), name='about'),
    url(r'^services/$', TemplateView.as_view(template_name='static_pages/services.html'), name='services'),
    url(r'^partners/$', TemplateView.as_view(template_name='static_pages/partners.html'), name='partners'),
    url(r'^bankruptcy/$', TemplateView.as_view(template_name='static_pages/bankruptcy.html'), name='bankruptcy'),
    url(r'^analitic/$', TemplateView.as_view(template_name='static_pages/analitic.html'), name='analitic'),
    url(r'^contacts/$', TemplateView.as_view(template_name='static_pages/contacts.html'), name='contacts'),
    url(r'^rules/$', TemplateView.as_view(template_name='static_pages/rules.html'), name='rules'),
    url(r'^price/$', TemplateView.as_view(template_name='static_pages/price.html'), name='price'),
    url(r'^secretary/$', TemplateView.as_view(template_name='static_pages/secretary.html'), name='secretary'),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
