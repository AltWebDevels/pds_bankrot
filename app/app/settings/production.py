from .base import *

DEBUG = False
TEMPLATE_DEBUG = DEBUG

try:
    from .local import *
except ImportError:
    pass
