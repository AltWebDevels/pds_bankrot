from .base import *

MIDDLEWARE_CLASSES += (
    'debug_toolbar.middleware.DebugToolbarMiddleware',
)

INSTALLED_APPS += (
    'debug_toolbar',
)

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

STATIC_ROOT = join(PROJECT_ROOT, '..', 'public', 'static')
MEDIA_ROOT = join(PROJECT_ROOT, '..', 'public', 'media')

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': os.environ.get('DB_NAME'),
        'USER': os.environ.get('DB_USER'),
        'PASSWORD': os.environ.get('DB_PASS'),
        'HOST': os.environ.get('POSTGRES_PORT_5432_TCP_ADDR'),
        'PORT': os.environ.get('POSTGRES_PORT_5432_TCP_PORT'),
    }
}

SMS_RU_KEY = 'f23dc940-24a5-fdb4-b53d-7ad810a329bd'
SMS_RU_SENDER = '79656660202'

BROKER_URL = os.getenv('REDIS_URL', 'redis://redis:6379/0')

from fnmatch import fnmatch


class glob_list(list):
    def __contains__(self, key):
        for elt in self:
            if fnmatch(key, elt): return True
        return False


INTERNAL_IPS = glob_list([
    '127.0.0.1',
    '192.168.*.*'
])

try:
    from .local import *
except ImportError:
    pass
