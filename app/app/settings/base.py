# -*- coding: utf-8 -*-

"""
Django settings for app project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
from os.path import abspath, dirname, join

gettext = lambda s: s
PROJECT_ROOT = dirname(dirname(dirname(abspath(__file__))))
BASE_DIR = PROJECT_ROOT
TEMPLATE_DIRS = (os.path.join(BASE_DIR, 'app', 'templates'),)
STATICFILES_DIRS = (os.path.join(BASE_DIR, 'static'),)

EMAIL_FROM = 'name@domain.com'
EMAIL_ADMIN = 'name@domain.com'
EMAIL_SUBJECT = 'New lots for you'

EMAIL_BACKEND = 'django_smtp_ssl.SSLEmailBackend'
EMAIL_HOST = 'smtp.server.com'
EMAIL_PORT = 465
EMAIL_USE_TLS = True
EMAIL_HOST_USER = 'name@domain.com'
EMAIL_HOST_PASSWORD = 'pass'

DEFAULT_FROM_EMAIL = EMAIL_HOST_USER
# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '79lycoleh*ow7qt7_**48#3s$kphsxkgp#y*)q$!ostf063fd0'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = DEBUG

ALLOWED_HOSTS = ['*']

SITE_ID = 1

# Application definition

INSTALLED_APPS = (
    # core
    'django.contrib.sites',
    'django.contrib.auth',
    'django.contrib.admin',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # contrib
    'widget_tweaks',
    # 'djcelery',
    # 'kombu.transport.django',
    # our
    'app',
    'auc_accounts',
    'auction',
    'registration',
    'forums',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.locale.LocaleMiddleware',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.contrib.messages.context_processors.messages',
    'django.core.context_processors.i18n',
    'django.core.context_processors.request',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
)

ROOT_URLCONF = 'app.urls'

WSGI_APPLICATION = 'app.wsgi.application'

ADMINS = ()

# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

# DATABASES = {
#     'default': dj_database_url.config(),
# }

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGES = [
    ('ru', 'Russian'),
]

LANGUAGE_CODE = 'ru-RU'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

# USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_ROOT = os.path.join(BASE_DIR, 'public', 'static')
STATIC_URL = '/static/'

MEDIA_ROOT = os.path.join(BASE_DIR, 'public', 'media')
MEDIA_URL = '/media/'

REGISTRATION_FORM = 'auc_accounts.forms.UserProfileRegistrationForm'
ACCOUNT_ACTIVATION_DAYS = 2

# celery
CELERYD_MAX_TASKS_PER_CHILD = 250
CELERY_RESULT_BACKEND = os.getenv('REDIS_URL', 'redis')
BROKER_URL = os.getenv('REDIS_URL', 'redis://localhost:6379/0')
# CELERY_TASK_RESULT_EXPIRES = None
# CELERY_ACCEPT_CONTENT = ['json']
# CELERY_TASK_SERIALIZER = 'json'
# CELERY_RESULT_SERIALIZER = 'json'

LOGIN_REDIRECT_URL = '/auc_accounts/profile'

# banklot settings
TEST_PREMIUM_DAYS = 15
MAX_PRICE_FOR_GUESTS = 100000

# msm.ru settings
SMS_RU_KEY = '00000000-0000-0000-0000-000000000000'
SMS_RU_SENDER = 'MyName'

CHRONOPAY_PRODUCT_ID = 'SECURE'
CHRONOPAY_SHARED_SEC = 'SECURE'
CHRONOPAY_REMOTE_IP = '185.30.16.166'
