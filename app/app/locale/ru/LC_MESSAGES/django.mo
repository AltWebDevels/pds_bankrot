��    2      �  C   <      H  z   I  1   �  q   �  �   h  �   �  %   �  e   �  f   E  �   �  �   i     ;	     M	     h	     ~	  
   �	     �	     �	     �	     �	     �	  5   �	     "
     8
  	   ?
     I
  
   P
  
   [
  &   f
  >   �
     �
     �
     �
     �
       =   1     o     x     �     �     �  B   �     �  ;        K  .   [     �     �  (   �     �  �  �  �   �  M   t  �   �  *  �  L  �  A   �  �   @  �   �  <  �  m  �  %   g  0   �  *   �  4   �       (   :     c  4        �     �  Z   �     ?     Z     g  
   �  
   �     �  6   �  l   �     V     u     �  (   �  +   �  ~        �  %   �     �     �  !   �  y        �  _   �  *     E   ,  ,   r  *   �  @   �              %       0   /               
              "       $           !       &   	                                               -      (         )      ,          2   '             *      1   +                          #      .           
    Forgot your password? Enter your email in the form below and we'll send you instructions for creating a new one.
     
    Sincerely,
    %(site_name)s Management
     
    To activate this account, please click the following link within the next
    %(expiration_days)s days:
     
    We have sent you an email with a link to reset your password. Please check
    your email and click the link to continue.
     
    You (or someone pretending to be you) have asked to register an account at
    %(site_name)s.  If this wasn't you, please ignore this email
    and your address will be removed from our records.
     
Sincerely,
%(site_name)s Management
 
To activate this account, please click the following link within the next
%(expiration_days)s days:
 
To reset your password, please click the following link, or copy and paste it
into your web browser:
 
You (or someone pretending to be you) have asked to register an account at
%(site_name)s.  If this wasn't you, please ignore this email
and your address will be removed from our records.
 
You are receiving this email because you (or someone pretending to be you)
requested that your password be reset on the %(domain)s site. If you do not
wish to reset your password, please ignore this message.
 Account Activated Account activation failed. Account activation on Activation email sent Add answer Best regards Change password Confirm password reset Create post Create topic Enter your new password below to reset your password: Forgot your password? Forums Greetings Log in Logged out Management No topics in this forum, create one :) No topics in this forum, only admins can create and post here. Not a member? Password changed Password reset Password reset complete Password successfully changed! Please check your email to complete the registration process. Register Registration is closed Reset it Reset password Set password Sorry, but registration is closed at this moment. Come back later. Successfully logged out This forum is closed, only admins can create and post here. You can log in. You may now <a href="%(login_url)s">log in</a> Your account is now activated. Your password has been reset! Your username, in case you've forgotten: registration Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-12-02 00:46+0000
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 
    Забыли свой пароль? Введите свой имейл в поле ниже и мы отправим Вам инструкции для создания нового.
     
    С уважением,
    администрация %(site_name)s
     
    Для активации этого аккаунта, пожалуйста, перейдите по ссылке в течении  следующих
    %(expiration_days)s дней:
     
    Мы отправили Вам письмо с ссылкой для активации Вашего аккаунта. Пожалуйста, проверьте
    проверьте свой почтовый ящик и перейдите по ссылке для продолжения.
     
    Вы (или кто-то притворившись вами) запросил регистрацию аккаунта на
    %(site_name)s. Если это были не Вы, пожалуйста, проигнорируйте это письмо
    и ваш имейл будет удален с нашей базы.
     
С уважением,
администрация %(site_name)s
 
Для активации этого аккаунта, пожалуйста, перейдите по ссылке в течении  следующих %(expiration_days)s дней:
 
Чтобы сбросить Ваш пароль, пожалуйста, перейдите по ссылке или скопируйте и вставьте ее
в Ваш веб браузер:
 
Вы (или кто-то притворившись вами) запросил регистрацию аккаунта на
%(site_name)s. Если это были не Вы, пожалуйста, проигнорируйте это письмо
и ваш имейл будет удален с нашей базы.
 
Вы получили это письмо так как Вы (или кто-то Вами притворившись)
сделал запрос на сброс Вашего пароля на сайте %(domain)s. Если Вы не желаете
сбросить ваш пароль, пожалуйста, проигнорируйте это сообщение.
 Аккаунт активирован Ошибка активации аккаунта Аккаунт активирован на Письмо активации отправлено Добавить ответ С лучшими пожеланиями Сменить пароль Подтверждение сброса пароля Создать пост Создать тему Введите свой новый пароль ниже для сброса пароля: Забыли пароль? Форумы Приветствуем, Логин Выход Администрация Нету тем здесь, создайте одну. В этом форуме нету тем, только админ может сюда публиковать Не пользователь? Пароль изменен Сбросить пароль Сброс пароля завершен Пароль успешно изменен! Проверьте, пожалуйста, ваш почтовый ящик для завершения регистрации. Регистрация Регистрация закрыта Сбросить Сброс пароля Установить пароль Извините, но регистрация на данный моент закрыта. Приходите позже. Успешный выход Этот форум зактрыт, только админ может постить сюда. Вы можете залогиниться Сейчас вы можете <a href="%(login_url)s">Войти</a> Аккаунт уже активирован Ваш пароль был сброшен! Ваш логин, на случай если вы забыли: регистрация 