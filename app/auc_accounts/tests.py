from django.test import TestCase, Client
from django.core.urlresolvers import reverse
from django.contrib.auth import authenticate
from django.contrib.auth.models import User


class GuestTestCase(TestCase):
    def setUp(self):
        self.client = Client()

    def test_guest_can_see_url_to_login(self):
        response = self.client.get('/', follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, reverse('auth_login'))

    def test_guest_can_see_url_to_registration(self):
        response = self.client.get(reverse('index'), follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, reverse('registration_register'))

    def test_guest_can_visit_register_page(self):
        response = self.client.get(reverse('registration_register'), follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'registration/registration_form.html')

    def test_guest_can_register(self):
        post_data = {
            'first_name': 'Bon',
            'last_name': 'Scott',
            'email': 'bscott@gmail.com',
            'phone': '+19876543',
            'city': 'NY',
            'password1': 'qwerty',
            'password2': 'qwerty',
        }
        response = self.client.post(reverse('registration_register'), post_data, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'registration/registration_complete.html')
        self.assertIsNot(authenticate(username='bscott@gmail.com', password='qwerty'), None)

    def test_guest_does_not_see_profile_link(self):
        response = self.client.get(reverse('index'), follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertNotContains(response, 'auc_accounts/profile')


class UserTestCase(TestCase):
    fixtures = ['test_data', ]
    username = 'vpupkin@gmail.com'
    password = 'pass1'

    def setUp(self):
        self.client = Client()
        self.client.login(username=self.username, password=self.password)

    def test_user_can_sign_up(self):
        self.client.logout()
        post_data = {
            'username': self.username,
            'password': self.password,
        }
        response = self.client.post(reverse('auc_accounts:login'), post_data, follow=True)
        self.assertTemplateUsed(response, 'auction/lot_list.html')
        self.assertContains(response, 'auc_accounts/profile')

    def test_user_can_see_url_to_his_profile(self):
        response = self.client.get('/', follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'auc_accounts/profile')

    def test_user_can_go_to_his_profile_page(self):
        response = self.client.get(reverse('auc_accounts:profile'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'auc_accounts/profile.html')

    def test_user_can_logout(self):
        response = self.client.get(reverse('auth_logout'), follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(self.client.session.session_key, '')

    def test_user_can_update_his_profile(self):
        post_data = {
            'first_name': 'Bon',
            'last_name': 'Scott',
            'email': 'bscott@gmail.com',
            'email_subscribe': 'on',
            'phone': '+19876543',
            'city': 'NY',
            'password': 'pass1',
            'password1': 'qwerty',
            'password2': 'qwerty',
        }
        response = self.client.post(reverse('auc_accounts:profile'), post_data, follow=True)
        self.assertEqual(response.status_code, 200)
        user = User.objects.get(username='bscott@gmail.com')
        self.assertEqual(user.email, 'bscott@gmail.com')

    def test_email_subscribe_unnecessary(self):
        post_data = {
            'first_name': 'Bon',
            'last_name': 'Scott',
            'email': 'bscott@gmail.com',
            'phone': '+19876543',
            'city': 'NY',
            'password': 'pass1',
            'password1': 'qwerty',
            'password2': 'qwerty',
        }
        response = self.client.post(reverse('auc_accounts:profile'), post_data, follow=True)
        self.assertEqual(response.status_code, 200)
        user = User.objects.get(username='bscott@gmail.com')
        self.assertEqual(user.email, 'bscott@gmail.com')

    def test_user_can_create_filter(self):
        pass

    def test_user_can_update_his_filter(self):
        pass

    def test_user_can_delete_his_filter(self):
        pass
