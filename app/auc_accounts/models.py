from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from django.utils import timezone


class UserProfile(models.Model):
    user = models.OneToOneField(User, primary_key=True, verbose_name=_('user'))
    email_subscribe = models.BooleanField(default=False, blank=True, verbose_name=_('email subscribe'))
    organization = models.BooleanField(default=False, blank=True, verbose_name=_('organization'))
    phone = models.CharField(max_length=200, verbose_name=_('phone'))
    city = models.CharField(max_length=200, verbose_name=_('city'), default='')
    ogrn = models.CharField(max_length=200, verbose_name=_('OGRN'), default='')
    premium_expiration_date = models.DateTimeField(default=timezone.now, verbose_name=_('Premium expiration date'))
    verified = models.BooleanField(default=False, blank=True, verbose_name=_('verified'))
    sms_token = models.IntegerField(verbose_name=_('sms token'), default=0)

    def is_premium(self):
        return self.verified and self.premium_expiration_date > timezone.now()

    def __str__(self):
        return self.user.username


class Order(models.Model):
    user = models.ForeignKey(User)
    create_date = models.DateTimeField(default=timezone.now, verbose_name=_('Order create date'))
    purchase_date = models.DateTimeField(null=True, default=None, verbose_name=_('Order purchase date'))
    price = models.IntegerField(verbose_name=_('price'), default=0)
    status = models.BooleanField(default=False, blank=True, verbose_name=_('purchased'))
    months = models.IntegerField(verbose_name=_('period'), default=0)

    def __str__(self):
        return '{} {}'.format(self.user.username, self.price)


class CbRequest(models.Model):
    request = models.TextField()
    create_date = models.DateTimeField(default=timezone.now, verbose_name=_('Order create date'))

    def __str__(self):
        return str(self.create_date)
