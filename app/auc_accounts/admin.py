from django.contrib import admin
from auc_accounts.models import UserProfile, Order, CbRequest

admin.site.register(UserProfile)
admin.site.register(Order)
admin.site.register(CbRequest)
