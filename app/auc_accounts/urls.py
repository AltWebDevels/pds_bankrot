from django.conf.urls import patterns, url
from auc_accounts.views import login_user, logout_user, user_profile, send_sms_ajax, verify_phone, order, create_order, \
    cb_callback, cb_error, cb_success
from django.views.generic import TemplateView

urlpatterns = patterns(
        '',
        url(r'^success/', TemplateView.as_view(template_name='auc_accounts/success.html'), name='success'),
        url(r'^login/', login_user, name='login'),
        url(r'^logout/', logout_user, name='logout'),
        url(r'^profile/', user_profile, name='profile'),
        url(r'^send_sms_ajax/(?P<user_id>\d+)/$', send_sms_ajax, name='send_sms_ajax'),
        url(r'^verify_phone/$', verify_phone, name='verify_phone'),
        url(r'^order/(?P<order_id>\d+)/$', order, name='order'),
        url(r'^create_order/$', create_order, name='create_order'),
        url(r'^cb_callback/$', cb_callback, name='cb_callback'),
        url(r'^cb_success/$', cb_success, name='cb_success'),
        url(r'^cb_error/$', cb_error, name='cb_error'),
)
