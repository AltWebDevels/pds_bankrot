from django.shortcuts import render_to_response, render
from django.template.context_processors import csrf
from auc_accounts.forms import UserProfileForm
from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.core.urlresolvers import reverse
from django.contrib.auth import logout, authenticate, login
from django.utils.translation import ugettext_lazy as _
from django.template import RequestContext
from auc_accounts.models import UserProfile, Order, CbRequest
from django.contrib.auth.decorators import login_required
from datetime import datetime, timedelta
from django.conf import settings
import json
import random
import re
import requests
import hashlib
from decimal import Decimal
from django.views.decorators.csrf import csrf_exempt
from django.utils import timezone


def login_user(request):
    logout(request)
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user and user.is_active:
            login(request, user)
            return HttpResponseRedirect(reverse('index'))
    return render_to_response('auc_accounts/login.html', context_instance=RequestContext(request))


def logout_user(request):
    logout(request)
    return HttpResponseRedirect(reverse('index'))


@login_required
def user_profile(request):
    profile, created = UserProfile.objects.get_or_create(user=request.user)
    form = None

    if request.method == 'POST':
        form = UserProfileForm(request.user, request.POST)
        if form.is_valid() and form.has_changed():
            form.save()
            return HttpResponseRedirect(reverse('auc_accounts:profile'))
    context = {
        'user_name': '{} {}'.format(request.user.first_name, request.user.last_name),
        'phone': profile.phone,
        'email': request.user.email,
        'orders': Order.objects.filter(user=request.user, status=False)
    }
    context.update(csrf(request))
    if form:
        context['form'] = form
    else:
        context['form'] = UserProfileForm(request.user, initial={
            'email_subscribe': profile.email_subscribe,
            'first_name': request.user.first_name,
            'last_name': request.user.last_name,
            'email': request.user.email,
            'phone': profile.phone,
            'city': profile.city,
            'ogrn': profile.ogrn,
        })
    return render(request, 'auc_accounts/profile.html', context)


def _send_sms(to, text):
    url = 'http://sms.ru/sms/send'
    params = {
        'api_id': settings.SMS_RU_KEY,
        'to': to,
        'text': text,
        'from': settings.SMS_RU_SENDER
    }
    if settings.DEBUG:
        params['test'] = 1

    r = requests.get(url, params=params)
    return r


@login_required
def send_sms_ajax(request, user_id):
    profile = UserProfile.objects.get(user_id=user_id)
    profile.sms_token = random.randint(1000, 9999)
    profile.phone = '+{}'.format(re.sub("[^0-9]", "", profile.phone))
    profile.save()
    res = _send_sms(profile.phone, profile.sms_token)
    result = {'success': False, 'message': res.text,}
    if res.text.startswith('100'):
        result['success'] = True

    return HttpResponse(json.dumps(result), content_type='application/json')


@login_required
def verify_phone(request):
    if request.user.userprofile.sms_token == int(request.POST.get('sms_code')):
        request.user.userprofile.verified = True
        request.user.userprofile.premium_expiration_date += timedelta(days=settings.TEST_PREMIUM_DAYS)
        request.user.userprofile.save()
    return HttpResponseRedirect(reverse('auc_accounts:profile'))


@login_required
def order(request, order_id):
    order = Order.objects.get(pk=order_id)
    product_id = settings.CHRONOPAY_PRODUCT_ID
    sharedsec = settings.CHRONOPAY_SHARED_SEC
    context = {
        'product_id': product_id,
        'sign': hashlib.md5('{}-{}-{}'.format(product_id, order.price, sharedsec).encode()).hexdigest(),
        'order': order,
    }
    return render(request, 'auc_accounts/order.html', context)


@login_required
def create_order(request):
    if request.method != 'POST':
        raise Http404
    months = int(request.POST.get('months'))
    price_per_month = 400
    if months >= 3:
        price_per_month = 350
    if months == 12:
        price = 4200
    else:
        price = price_per_month * months
    order = Order.objects.create(
            user=request.user,
            price=price,
            months=months,
    )
    return HttpResponseRedirect(reverse('auc_accounts:order', kwargs={'order_id': order.id}))


@csrf_exempt
def cb_callback(request):
    request_json = json.dumps({
        'GET': repr(request.GET),
        'POST': repr(request.POST),
        'META': repr(request.META),
    })
    CbRequest.objects.create(request=request_json)

    if request.META.get('REMOTE_ADDR') != settings.CHRONOPAY_REMOTE_IP:
        raise Http404

    sign = hashlib.md5('{}{}{}{}{}'.format(
            settings.CHRONOPAY_SHARED_SEC,
            request.POST.get('customer_id'),
            request.POST.get('transaction_id'),
            request.POST.get('transaction_type'),
            request.POST.get('total'),
    ).encode()).hexdigest()

    if sign != request.POST.get('sign'):
        raise Http404

    order = Order.objects.get(id=request.POST.get('cs1'))
    order.status = True
    order.purchase_date = timezone.now()
    order.save()
    if order.user.userprofile.premium_expiration_date < timezone.now():
        order.user.userprofile.premium_expiration_date = timezone.now()

    order.user.userprofile.premium_expiration_date += timedelta(order.months * 30)

    order.user.userprofile.save()

    return HttpResponse(json.dumps({'result': 'OK'}), content_type='application/json')


@csrf_exempt
def cb_success(request):
    return render(request, 'auc_accounts/cb_success.html')


@csrf_exempt
def cb_error(request):
    return render(request, 'auc_accounts/cb_error.html')
