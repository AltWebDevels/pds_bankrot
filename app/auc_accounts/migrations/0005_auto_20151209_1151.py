# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('auc_accounts', '0004_userprofile_city'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='ogrn',
            field=models.CharField(verbose_name='OGRN', max_length=200, default=''),
        ),
        migrations.AddField(
            model_name='userprofile',
            name='organization',
            field=models.BooleanField(verbose_name='organization', default=False),
        ),
        migrations.AddField(
            model_name='userprofile',
            name='premium_expiration_date',
            field=models.DateTimeField(verbose_name='Premium expiration date', default=django.utils.timezone.now),
        ),
        migrations.AddField(
            model_name='userprofile',
            name='sms_token',
            field=models.IntegerField(verbose_name='sms token', default=0),
        ),
        migrations.AddField(
            model_name='userprofile',
            name='verified',
            field=models.BooleanField(verbose_name='verified', default=False),
        ),
    ]
