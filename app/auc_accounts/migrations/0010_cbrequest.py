# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('auc_accounts', '0009_order_price'),
    ]

    operations = [
        migrations.CreateModel(
            name='CbRequest',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, primary_key=True, auto_created=True)),
                ('request', models.TextField()),
                ('create_date', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Order create date')),
            ],
        ),
    ]
