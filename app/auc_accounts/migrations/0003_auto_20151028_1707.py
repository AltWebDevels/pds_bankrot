# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('auc_accounts', '0002_auto_20150404_0939'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofile',
            name='email_subscribe',
            field=models.BooleanField(verbose_name='email subscribe', default=False),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='phone',
            field=models.CharField(verbose_name='phone', max_length=200),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='user',
            field=models.OneToOneField(serialize=False, primary_key=True, verbose_name='user', to=settings.AUTH_USER_MODEL),
        ),
    ]
