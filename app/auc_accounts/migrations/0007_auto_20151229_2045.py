# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('auc_accounts', '0006_order'),
    ]

    operations = [
        migrations.RenameField(
            model_name='order',
            old_name='mounts',
            new_name='months',
        ),
    ]
