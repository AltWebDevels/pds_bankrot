# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('auc_accounts', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='userprofile',
            old_name='name',
            new_name='phone',
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='email_subscribe',
            field=models.BooleanField(default=False),
        ),
    ]
