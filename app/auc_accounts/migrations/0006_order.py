# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('auc_accounts', '0005_auto_20151209_1151'),
    ]

    operations = [
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', serialize=False, primary_key=True)),
                ('create_date', models.DateTimeField(verbose_name='Order create date', default=django.utils.timezone.now)),
                ('purchase_date', models.DateTimeField(null=True, verbose_name='Order purchase date', default=None)),
                ('price', models.DecimalField(decimal_places=2, null=True, verbose_name='price', max_digits=5)),
                ('status', models.BooleanField(verbose_name='purchased', default=False)),
                ('mounts', models.IntegerField(verbose_name='period', default=0)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
