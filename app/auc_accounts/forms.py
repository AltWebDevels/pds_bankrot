from django import forms
from django.conf import settings
from auc_accounts.models import UserProfile
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.utils.translation import ugettext_lazy as _
from registration.forms import RegistrationFormUniqueEmail
from datetime import datetime, timedelta
import re


class UserProfileRegistrationForm(RegistrationFormUniqueEmail):
    organization = forms.BooleanField(label=_('As organization?'), initial=False, required=False)
    phone = forms.CharField(max_length=100, label=_('phone'))
    city = forms.CharField(max_length=100, label=_('city'))
    ogrn = forms.CharField(max_length=100, label=_('OGRN'), required=False)

    class Meta:
        model = User
        fields = ('organization', 'first_name', 'ogrn', 'last_name', 'email')

    def clean_ogrn(self):
        if self.cleaned_data['organization'] and not self.cleaned_data['ogrn']:
            raise forms.ValidationError(_("This field is required."))
        return self.cleaned_data['ogrn']

    def save(self, commit=True):
        user = super(UserProfileRegistrationForm, self).save(commit=False)
        user.username = self.cleaned_data['email']
        user.save()
        user_profile = UserProfile()
        user_profile.user = user
        user_profile.phone = self.cleaned_data['phone']
        user_profile.city = self.cleaned_data['city']
        if self.cleaned_data['organization']:
            user_profile.organization = True
            user_profile.ogrn = self.cleaned_data['ogrn']
            user_profile.premium_expiration_date = datetime.now() + timedelta(days=settings.TEST_PREMIUM_DAYS)
        user_profile.save()
        return user


class UserProfileForm(forms.Form):
    email_subscribe = forms.BooleanField(label=_('email subscribe'), initial=True, required=False)
    first_name = forms.CharField(max_length=100, label=_('first name'))
    last_name = forms.CharField(max_length=100, label=_('last name'), required=False)
    email = forms.EmailField(label=_('email'))
    phone = forms.CharField(max_length=20, label=_('phone'))
    ogrn = forms.CharField(max_length=20, label=_('ogrn'), required=False)
    city = forms.CharField(max_length=20, label=_('city'))
    password = forms.CharField(max_length=20, required=False, label=_('password'))
    password1 = forms.CharField(max_length=20, required=False, label=_('new password'))
    password2 = forms.CharField(max_length=20, required=False, label=_('repeat password'))

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super(UserProfileForm, self).__init__(*args, **kwargs)

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if password1 != password2:
            raise forms.ValidationError(_('Your passwords do not match'))
        return password2

    def clean_password(self):
        if not self.cleaned_data.get('password'):
            return False
        if not authenticate(username=self.user.username, password=self.cleaned_data.get('password')):
            raise forms.ValidationError(_('Wrong password'))

    def clean_phone(self):
        if not self.cleaned_data.get('phone'):
            raise forms.ValidationError(_("This field is required."))
        return '+{}'.format(re.sub("[^0-9]", "", self.cleaned_data.get('phone')))

    def save(self):
        user = User.objects.get(pk=self.user.id)
        profile = UserProfile.objects.get(pk=self.user)
        profile.email_subscribe = self.cleaned_data.get('email_subscribe')
        if not profile.phone or profile.organization:
            profile.phone = self.cleaned_data.get('phone')
        profile.city = self.cleaned_data.get('city')
        user.email = self.cleaned_data.get('email')
        user.username = self.cleaned_data.get('email')
        user.first_name = self.cleaned_data.get('first_name')
        user.last_name = self.cleaned_data.get('last_name')
        if self.cleaned_data.get('password1'):
            user.set_password(self.cleaned_data.get('password1'))

        user.save()
        profile.save()
