import re
from django import template
from auction.forms import SearchForm
from auction.models import Lot, UserData
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.conf import settings

register = template.Library()


@register.inclusion_tag('auction/search_form.html', takes_context=True)
def search_form(context):
    tag_context = {
        'form': context.get('form') if context.get('form') else SearchForm(),
        'user': context.request.user,
    }
    return tag_context


@register.inclusion_tag('auction/lot_list.html', takes_context=True)
def lot_list(context):
    lot_set = context.get('lot_set')
    paginator = Paginator(lot_set, 10)

    page = context.request.GET.get('page')
    try:
        lot_set = paginator.page(page)
        page = int(page)
    except PageNotAnInteger:
        lot_set = paginator.page(1)
        page = 1
    except EmptyPage:
        lot_set = paginator.page(paginator.num_pages)
        page = paginator.num_pages

    range_gap = 3

    if page > range_gap:
        start = page - range_gap
    else:
        start = 1

    if page < paginator.num_pages - range_gap:
        end = page + range_gap + 1
    else:
        end = paginator.num_pages + 1

    tag_context = {
        'page_range': range(start, end),
        'user': context.get('request').user,
        'lot_set': lot_set,
        'chapter_set': context.get('chapter_set'),
        'MEDIA_URL': context.get('MEDIA_URL'),
        'MAX_PRICE_FOR_GUESTS': settings.MAX_PRICE_FOR_GUESTS,
        'search': context.get('request').META.get('QUERY_STRING'),
    }
    if tag_context['search'].startswith('page'):
        tag_context['search'] = re.sub("page=\d+&", "", tag_context['search'])

    if not 'search_form' in tag_context['search']:
        tag_context['search'] = None

    return tag_context


@register.simple_tag
def get_chapters_lot_count(user_id, chapter_id):
    chapter_count = Lot.objects.filter(users_data=user_id, userdata__chapter_id=chapter_id).count()
    return chapter_count


@register.assignment_tag
def get_chapter(user_id, lot_id):
    try:
        return UserData.objects.filter(user_id=user_id, lot_id=lot_id)[0].chapter_id
    except IndexError:
        return False


@register.simple_tag
def hide_text(str, lot, user=None):
    if user.id and lot.price_start and int(lot.price_start) < settings.MAX_PRICE_FOR_GUESTS:
        return str
    if user.id and user.userprofile.is_premium() or not str:
        return str
    return ''.join([c if c in '/ \\-' else '&#9618;' for c in str])
