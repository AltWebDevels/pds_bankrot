from django import forms
from auction.models import Marketplace, LotType, Chapter, Filter, Region, Question
from django.utils.translation import ugettext_lazy as _


class SearchForm(forms.Form):
    search = forms.CharField(max_length=100, required=False, label=_('search'))
    is_visited = forms.BooleanField(required=False, label=_('is visited'))
    places = forms.ModelMultipleChoiceField(queryset=Marketplace.objects.all().distinct(), required=False,
                                            label=_('places'))
    region = forms.CharField(max_length=100, required=False, label=_('region'))
    price1 = forms.CharField(max_length=100, required=False, label=_('price from'))
    price2 = forms.CharField(max_length=100, required=False, label=_('price to'))
    date1 = forms.CharField(max_length=100, required=False, label=_('date from'))
    date2 = forms.CharField(max_length=100, required=False, label=_('date to'))
    types = forms.ModelMultipleChoiceField(queryset=LotType.objects.all().distinct(), required=False,
                                           label=_('lot types'))
    debtor = forms.CharField(max_length=100, required=False, label=_('debtor'))
    trustee = forms.CharField(max_length=100, required=False, label=_('trustee'))


class AddChapterForm(forms.Form):
    title = forms.CharField(max_length=100, label=_('title'))

    def save(self, user):
        chapter = Chapter(title=self.cleaned_data.get('title'), user=user)
        chapter.save()


class FilterForm(forms.ModelForm):
    lot_types = forms.ModelMultipleChoiceField(
        widget=forms.CheckboxSelectMultiple(),
        queryset=LotType.objects.all(),
        required=False,
    )
    regions = forms.ModelMultipleChoiceField(
        widget=forms.CheckboxSelectMultiple(),
        queryset=Region.objects.all(),
        required=False,
    )

    class Meta:
        model = Filter
        exclude = ['user']


class QuestionForm(forms.ModelForm):
    class Meta:
        model = Question
        exclude = []
