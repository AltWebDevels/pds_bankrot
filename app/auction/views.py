from django.shortcuts import render, get_object_or_404
from auction.models import Lot, UserData, Chapter, Filter
from auction.forms import SearchForm, AddChapterForm, FilterForm, QuestionForm
from django.http import HttpResponseRedirect, HttpResponse, HttpResponseNotFound
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from datetime import datetime
from auction.tasks import send_email_task
import json


def get_default_chapter(user_id):
    chapter, created = Chapter.objects.get_or_create(user_id=user_id, title='Разное')
    if created:
        for ud in UserData.objects.filter(user_id=user_id, chapter_id=None):
            ud.chapter = chapter
            ud.save()
    return chapter


def lot_list(request):
    form = SearchForm(request.GET) if request.GET.get('search_form') else SearchForm()

    lot_objects = Lot.objects.filter(active=True, date_finish__gte=datetime.now()).order_by('-date_message')
    # if not request.user.id or not request.user.userprofile.is_premium():
    # lot_objects = lot_objects.filter(price_start__lte=settings.MAX_PRICE_FOR_GUESTS)

    if request.GET.get('search_form') and form.is_valid():
        if form.cleaned_data.get('search'):
            lot_objects = lot_objects.filter(description__icontains=form.cleaned_data.get('search'))
        if not form.cleaned_data.get('is_visited') and request.user.is_authenticated():
            lot_objects = lot_objects.exclude(users_data=request.user.id, userdata__read=True)
        if form.cleaned_data.get('region'):
            lot_objects = lot_objects.filter(debtor__region__title__icontains=form.cleaned_data.get('region'))
        if form.cleaned_data.get('places'):
            lot_objects = lot_objects.filter(marketplace__in=form.cleaned_data.get('places'))
        if form.cleaned_data.get('types'):
            lot_objects = lot_objects.filter(lot_type__in=form.cleaned_data.get('types'))
        if form.cleaned_data.get('price1'):
            lot_objects = lot_objects.filter(price_start__gte=form.cleaned_data.get('price1'))
        if form.cleaned_data.get('price2'):
            lot_objects = lot_objects.filter(price_start__lte=form.cleaned_data.get('price2'))
        if form.cleaned_data.get('date1'):
            date_obj = datetime.strptime(form.cleaned_data.get('date1'), '%m/%d/%Y')
            lot_objects = lot_objects.filter(date_start__gte=date_obj)
        if form.cleaned_data.get('date2'):
            date_obj = datetime.strptime(form.cleaned_data.get('date2'), '%m/%d/%Y')
            lot_objects = lot_objects.filter(date_finish__gte=date_obj)
        if form.cleaned_data.get('debtor'):
            lot_objects = lot_objects.filter(debtor__title__icontains=form.cleaned_data.get('debtor'))
        if form.cleaned_data.get('trustee'):
            lot_objects = lot_objects.filter(trustee__name__icontains=form.cleaned_data.get('trustee'))

    elif request.user.is_authenticated():
        lot_objects = lot_objects.exclude(users_data=request.user.id, userdata__read=True)

    chapters = []
    if request.user.is_authenticated():
        chapters = Chapter.objects.filter(user=request.user.id)
    context = {
        'lot_set': lot_objects,
        'form': form,
        'chapter_set': chapters,
    }
    return render(request, 'auction/auction.html', context)


def lot_details(request, pk):
    lot = get_object_or_404(Lot, pk=pk)
    chapters = []
    user_logged = request.user.is_authenticated()
    if user_logged:
        chapters = Chapter.objects.filter(user=request.user.id)

    context = {
        'lot': lot,
        'lots_active': Lot.objects.filter(debtor__pk=lot.debtor_id, active=True,
                                          date_finish__gte=datetime.now()).exclude(pk=lot.id),
        'lots_finished': Lot.objects.filter(debtor__pk=lot.debtor_id, active=True,
                                            date_finish__lte=datetime.now()).exclude(pk=lot.id),
        'chapter_set': chapters,
        'show': False,
    }

    if user_logged and lot.price_start and int(lot.price_start) < settings.MAX_PRICE_FOR_GUESTS or \
                    request.user.id and request.user.userprofile.is_premium():
        context['show'] = True

    return render(request, 'auction/lot_detail.html', context)


@login_required
def toggle_lot_read_ajax(request, lot_id):
    try:
        lot = get_object_or_404(Lot, pk=lot_id)
        user_data, created = UserData.objects.get_or_create(user=request.user, lot=lot)
        user_data.read = not user_data.read
        user_data.save()
        result = {'success': True}
    except:
        result = {'success': False}
    return HttpResponse(json.dumps(result), content_type='application/json')


@login_required
def read_all(request):
    lot_objects = Lot.objects.filter(
            date_finish__gte=datetime.now()
    ).exclude(userdata__read=True)
    for lot in lot_objects:
        userdata, created = UserData.objects.get_or_create(
                user_id=request.user.id,
                lot_id=lot.id
        )
        userdata.read = True
        userdata.save()

    return HttpResponseRedirect(reverse('index'))


@login_required
def favorite_list(request, id=None):
    get_default_chapter(request.user.id)
    lot_queryset = Lot.objects.filter(users_data=request.user.id).exclude(userdata__chapter=None)
    if id:
        lot_queryset = lot_queryset.filter(userdata__chapter=id)
    chapter_set = Chapter.objects.filter(user=request.user.id)
    context = {
        'chapter_set': chapter_set,
        'add_form': AddChapterForm(),
        'lot_set': lot_queryset,
    }
    return render(request, 'auction/favorite.html', context)


@login_required
def add_chapter(request):
    if request.method == 'POST':
        form = AddChapterForm(request.POST)
        if form.is_valid():
            form.save(request.user)
    return HttpResponseRedirect(reverse('auction:favorite_list'))


@login_required
def del_chapter(request, id):
    chapter = get_object_or_404(Chapter, pk=id)
    chapter.delete()
    return HttpResponseRedirect(reverse('auction:favorite_list'))


@login_required
def get_comment(request, lot_id):
    try:
        user_data = get_object_or_404(UserData, lot_id=lot_id, user_id=request.user.id)
        data = {
            'comment': user_data.comment
        }
    except:
        data = {
            'comment': None
        }
    return HttpResponse(json.dumps(data), content_type='application/json')


@login_required
def set_comment(request, lot_id):
    if request.method == 'POST':
        lot = get_object_or_404(Lot, pk=lot_id)
        user_data, created = UserData.objects.get_or_create(user=request.user, lot=lot)
        user_data.comment = request.POST['comment']
        if not user_data.chapter:
            user_data.chapter = get_default_chapter(user_id=request.user.id)
        user_data.save()
        return HttpResponse(json.dumps({'result': True}), content_type='application/json')
    return HttpResponse(json.dumps({'result': False}), content_type='application/json')


@login_required
def del_comment(request, lot_id):
    lot = get_object_or_404(Lot, pk=lot_id)
    user_data = UserData(lol=lot, user=request.user)
    user_data.comment = ''
    user_data.save()
    return HttpResponse(json.dumps({'result': True}), content_type='application/json')


@login_required
def add_to_chapter(request):
    try:
        lot_id = int(request.GET.get('auctionId'))
        chapter_id = int(request.GET.get('categoryId'))
    except:
        return HttpResponseNotFound()

    user_data, created = UserData.objects.get_or_create(user_id=request.user.id, lot_id=lot_id)
    user_data.chapter = get_object_or_404(Chapter, pk=chapter_id)
    user_data.save()
    return HttpResponse(json.dumps({'result': True}), content_type='application/json')


@login_required
def remove_from_chapter(request):
    try:
        lot_id = int(request.GET.get('auctionId'))
    except:
        return HttpResponseNotFound()

    user_data, created = UserData.objects.get_or_create(user_id=request.user.id, lot_id=lot_id)
    user_data.chapter = None
    user_data.save()
    return HttpResponse(json.dumps({'result': True}), content_type='application/json')


@login_required
def calendar(request):
    context = {}
    return render(request, 'auction/calendar.html', context)


@login_required
def data_by_month(request, year, month):
    lot_queryset = Lot.objects \
        .filter(users_data=request.user.id) \
        .filter(date_app_to__year=year) \
        .filter(date_app_to__month=month) \
        .exclude(userdata__chapter=None)
    result = {}
    for lot in lot_queryset:
        user_data, created = UserData.objects.get_or_create(user=request.user, lot=lot)
        data = {
            'number': lot.number,
            'url': lot.get_absolute_url(),
            'lot_id': lot.id,
            'comment': user_data.comment,
        }
        try:
            result[lot.date_app_to.day].append(data)
        except KeyError:
            result[lot.date_app_to.day] = [data, ]
    return HttpResponse(json.dumps(result), content_type='application/json')


@login_required
def monitoring(request):
    context = {
        'filters': Filter.objects.filter(user=request.user.id)
    }
    return render(request, 'auction/monitoring.html', context)


@login_required
def filter_edit(request, filter_id=None):
    form = None
    filter = None
    filter_id = filter_id if filter_id else request.POST.get('filter_id')

    if filter_id:
        filter = Filter.objects.get(pk=filter_id)

    if request.method == 'POST':
        form = FilterForm(request.POST, instance=filter)

        if form.is_valid() and form.has_changed():
            filter = form.save(commit=True)
            filter.user = request.user
            filter.save()
            return HttpResponseRedirect(reverse('auction:monitoring'))
    context = {
        'form': form if form else FilterForm(instance=filter),
        'filter': filter
    }
    return render(request, 'auction/filter_edit.html', context)


def question_form(request):
    context = {}
    form = None
    if request.method == 'POST':
        form = QuestionForm(request.POST)
        if form.is_valid():
            form.save()
            send_email_task.delay('New question', _('Email has been sent'), settings.EMAIL_HOST_USER,
                                  [settings.EMAIL_ADMIN, ], fail_silently=True)
            context['message'] = _('Your message has been sent!')
            form = None
    context['form'] = form if form else QuestionForm()
    return render(request, 'auction/question_form.html', context)


def blog(request):
    context = {}

    return render(request, 'auction/blog.html', context)


def blog_theme(request):
    context = {}

    return render(request, 'auction/blog_theme.html', context)


def blog_question(request):
    context = {}

    return render(request, 'auction/blog_question.html', context)
