from django.contrib import admin
from auction.models import *


class LotAdmin(admin.ModelAdmin):
    list_display = ['number', 'active']


admin.site.register(Lot, LotAdmin)


class DebtorAdmin(admin.ModelAdmin):
    pass


admin.site.register(Debtor, DebtorAdmin)


class RegionAdmin(admin.ModelAdmin):
    pass


admin.site.register(Region, RegionAdmin)


class LotMessageAdmin(admin.ModelAdmin):
    pass


admin.site.register(LotMessage, LotMessageAdmin)


class DebtorMessageAdmin(admin.ModelAdmin):
    pass


admin.site.register(DebtorMessage, DebtorMessageAdmin)


class MarketplaceAdmin(admin.ModelAdmin):
    pass


admin.site.register(Marketplace, MarketplaceAdmin)


class CurrencyAdmin(admin.ModelAdmin):
    pass


admin.site.register(Currency, CurrencyAdmin)


class LotTypeAdmin(admin.ModelAdmin):
    pass


admin.site.register(LotType, LotTypeAdmin)


class ChapterAdmin(admin.ModelAdmin):
    pass


admin.site.register(Chapter, ChapterAdmin)


class DocumentAdmin(admin.ModelAdmin):
    pass


admin.site.register(Document, DocumentAdmin)


class FilterAdmin(admin.ModelAdmin):
    pass


admin.site.register(Filter, FilterAdmin)


class TrusteeAdmin(admin.ModelAdmin):
    pass


admin.site.register(Trustee, TrusteeAdmin)


class OrganizerAdmin(admin.ModelAdmin):
    pass


admin.site.register(Organizer, OrganizerAdmin)


class UserDataAdmin(admin.ModelAdmin):
    pass


admin.site.register(UserData, UserDataAdmin)


class PriceAdmin(admin.ModelAdmin):
    pass


admin.site.register(Price, PriceAdmin)
admin.site.register(Question)
