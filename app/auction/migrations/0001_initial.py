# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Chapter',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('title', models.CharField(max_length=200, null=True, verbose_name='title')),
                ('user', models.ForeignKey(null=True, to=settings.AUTH_USER_MODEL, verbose_name='user')),
            ],
            options={
                'verbose_name_plural': 'chapters',
                'verbose_name': 'chapter',
            },
        ),
        migrations.CreateModel(
            name='Currency',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('title', models.CharField(max_length=200, null=True, verbose_name='title')),
                ('sign', models.CharField(max_length=4, null=True, verbose_name='sign')),
            ],
            options={
                'verbose_name_plural': 'currencies',
                'verbose_name': 'currency',
            },
        ),
        migrations.CreateModel(
            name='Debtor',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('title', models.CharField(unique=True, max_length=300, null=True, verbose_name='title')),
                ('vatin', models.CharField(max_length=20, null=True, verbose_name='vatin')),
                ('register_id', models.CharField(max_length=200, null=True, verbose_name='register id')),
                ('address', models.CharField(max_length=500, null=True, verbose_name='address')),
                ('fed_url', models.URLField(unique=True, null=True, verbose_name='fed url')),
            ],
            options={
                'verbose_name_plural': 'debtors',
                'verbose_name': 'debtor',
            },
        ),
        migrations.CreateModel(
            name='DebtorMessage',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('title', models.CharField(max_length=200, null=True, verbose_name='title')),
                ('date', models.DateTimeField(verbose_name='date', null=True)),
                ('fed_url', models.URLField(unique=True, null=True, verbose_name='source')),
                ('debtor', models.ForeignKey(null=True, to='auction.Debtor', verbose_name='debtor')),
            ],
            options={
                'verbose_name_plural': 'debtor messages',
                'verbose_name': 'debtor message',
            },
        ),
        migrations.CreateModel(
            name='Document',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('title', models.CharField(max_length=200, null=True, verbose_name='title')),
                ('fed_url', models.URLField(unique=True, null=True, verbose_name='url')),
                ('date', models.DateTimeField(verbose_name='date', null=True)),
            ],
            options={
                'verbose_name_plural': 'documents',
                'verbose_name': 'document',
            },
        ),
        migrations.CreateModel(
            name='Filter',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('is_active', models.BooleanField(default=False, verbose_name='active')),
                ('title', models.CharField(max_length=200, null=True, verbose_name='title')),
                ('key_words', models.TextField(blank=True, max_length=100, null=True, verbose_name='keywords')),
                ('is_all_keywords_necessary', models.BooleanField(default=False, verbose_name='all keywords necessary')),
                ('price_from', models.FloatField(blank=True, verbose_name='price from', null=True)),
                ('price_to', models.FloatField(blank=True, verbose_name='price to', null=True)),
                ('email_info', models.BooleanField(default=False, verbose_name='email info')),
                ('email_period', models.IntegerField(blank=True, choices=[(1, 'hourly'), (2, 'daily'), (3, 'weekly')], verbose_name='email period', null=True)),
                ('sms_info', models.BooleanField(default=False, verbose_name='sms info')),
                ('debtors', models.ManyToManyField(to='auction.Debtor', blank=True, related_name='_debtors_+', verbose_name='debtors')),
            ],
            options={
                'verbose_name_plural': 'filters',
                'verbose_name': 'filter',
            },
        ),
        migrations.CreateModel(
            name='Lot',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('active', models.BooleanField(default=False, verbose_name='active')),
                ('number', models.CharField(unique=True, max_length=200, null=True, verbose_name='number')),
                ('fed_url', models.URLField(unique=True, null=True, verbose_name='fed url')),
                ('marketplace_url', models.URLField(null=True, verbose_name='marketplace url')),
                ('description', models.TextField(verbose_name='description', null=True)),
                ('date_message', models.DateTimeField(verbose_name='placement date', null=True)),
                ('date_start', models.DateTimeField(verbose_name='trading start date', null=True)),
                ('date_finish', models.DateTimeField(verbose_name='trading finish date', null=True)),
                ('date_app_from', models.DateTimeField(verbose_name='apply date from', null=True)),
                ('date_app_to', models.DateTimeField(verbose_name='apply date to', null=True)),
                ('case_number', models.CharField(max_length=200, null=True, verbose_name='case number')),
                ('cort', models.CharField(max_length=200, null=True, verbose_name='cort')),
                ('price_start', models.DecimalField(decimal_places=2, verbose_name='price start', null=True, max_digits=15)),
                ('currency', models.ForeignKey(null=True, to='auction.Currency', verbose_name='currency')),
                ('debtor', models.ForeignKey(null=True, to='auction.Debtor', verbose_name='debtor')),
            ],
            options={
                'verbose_name_plural': 'lots',
                'verbose_name': 'lot',
            },
        ),
        migrations.CreateModel(
            name='LotMessage',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('title', models.CharField(max_length=200, null=True, verbose_name='title')),
                ('date', models.DateTimeField(verbose_name='date', null=True)),
                ('fed_url', models.URLField(unique=True, null=True, verbose_name='source')),
                ('lot', models.ForeignKey(null=True, to='auction.Lot', verbose_name='lot')),
            ],
            options={
                'verbose_name_plural': 'lot messages',
                'verbose_name': 'lot message',
            },
        ),
        migrations.CreateModel(
            name='LotType',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('title', models.CharField(unique=True, max_length=200, null=True, verbose_name='title')),
                ('code', models.CharField(max_length=200, null=True, verbose_name='code')),
            ],
            options={
                'verbose_name_plural': 'lot types',
                'verbose_name': 'lot type',
            },
        ),
        migrations.CreateModel(
            name='Marketplace',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('title', models.CharField(unique=True, max_length=200, null=True, verbose_name='title')),
                ('url', models.URLField(null=True, verbose_name='url')),
                ('img', models.ImageField(upload_to='', null=True, verbose_name='image')),
                ('fed_url', models.URLField(unique=True, null=True, verbose_name='fed url')),
            ],
            options={
                'verbose_name_plural': 'marketplaces',
                'verbose_name': 'marketplace',
            },
        ),
        migrations.CreateModel(
            name='Organizer',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('vatin', models.CharField(max_length=200, null=True, verbose_name='vatin')),
                ('name', models.CharField(max_length=200, null=True, verbose_name='name')),
                ('fed_url', models.URLField(unique=True, null=True, verbose_name='fed url')),
            ],
            options={
                'verbose_name_plural': 'organizers',
                'verbose_name': 'organizer',
            },
        ),
        migrations.CreateModel(
            name='Price',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('value', models.DecimalField(decimal_places=2, verbose_name='value', null=True, max_digits=15)),
                ('date', models.DateTimeField(verbose_name='date', null=True)),
                ('lot', models.ForeignKey(related_name='price_set', null=True, to='auction.Lot', verbose_name='lot')),
            ],
            options={
                'verbose_name_plural': 'prices',
                'verbose_name': 'price',
            },
        ),
        migrations.CreateModel(
            name='Question',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('name', models.CharField(max_length=100, null=True, verbose_name='name')),
                ('email', models.EmailField(max_length=100, null=True, verbose_name='email')),
                ('phone', models.CharField(max_length=100, null=True, verbose_name='phone')),
                ('message', models.TextField(blank=True, max_length=100, null=True, verbose_name='message')),
            ],
            options={
                'verbose_name_plural': 'questions',
                'verbose_name': 'question',
            },
        ),
        migrations.CreateModel(
            name='Region',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('title', models.CharField(max_length=200, null=True, verbose_name='title')),
            ],
            options={
                'verbose_name_plural': 'regions',
                'verbose_name': 'region',
            },
        ),
        migrations.CreateModel(
            name='Status',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('title', models.CharField(max_length=200, null=True, verbose_name='title')),
            ],
            options={
                'verbose_name_plural': 'statuses',
                'verbose_name': 'status',
            },
        ),
        migrations.CreateModel(
            name='Trustee',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('vatin', models.CharField(max_length=200, null=True, verbose_name='vatin')),
                ('name', models.CharField(max_length=200, null=True, verbose_name='name')),
                ('fed_url', models.URLField(unique=True, null=True, verbose_name='fed url')),
            ],
            options={
                'verbose_name_plural': 'trustees',
                'verbose_name': 'trustee',
            },
        ),
        migrations.CreateModel(
            name='UserData',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('comment', models.TextField(verbose_name='comment', null=True)),
                ('read', models.BooleanField(default=False, verbose_name='read')),
                ('sent', models.BooleanField(default=False, verbose_name='sent')),
                ('chapter', models.ForeignKey(null=True, to='auction.Chapter', verbose_name='chapter')),
                ('lot', models.ForeignKey(null=True, to='auction.Lot', verbose_name='lot')),
                ('user', models.ForeignKey(null=True, to=settings.AUTH_USER_MODEL, verbose_name='user')),
            ],
            options={
                'verbose_name_plural': 'user data',
                'verbose_name': 'user data',
            },
        ),
        migrations.AddField(
            model_name='lot',
            name='lot_type',
            field=models.ForeignKey(null=True, to='auction.LotType', verbose_name='lot type'),
        ),
        migrations.AddField(
            model_name='lot',
            name='marketplace',
            field=models.ForeignKey(null=True, to='auction.Marketplace', verbose_name='marketplace'),
        ),
        migrations.AddField(
            model_name='lot',
            name='organizer',
            field=models.ForeignKey(null=True, to='auction.Organizer', verbose_name='organizer'),
        ),
        migrations.AddField(
            model_name='lot',
            name='status',
            field=models.ForeignKey(to='auction.Status', null=True),
        ),
        migrations.AddField(
            model_name='lot',
            name='trustee',
            field=models.ForeignKey(null=True, to='auction.Trustee', verbose_name='trustee'),
        ),
        migrations.AddField(
            model_name='lot',
            name='users_data',
            field=models.ManyToManyField(to=settings.AUTH_USER_MODEL, blank=True, related_name='_users_data_+', through='auction.UserData', verbose_name='user data'),
        ),
        migrations.AddField(
            model_name='filter',
            name='lot_types',
            field=models.ManyToManyField(to='auction.LotType', blank=True, related_name='_lot_types_+', verbose_name='lot types'),
        ),
        migrations.AddField(
            model_name='filter',
            name='regions',
            field=models.ManyToManyField(to='auction.Region', blank=True, related_name='_regions_+', verbose_name='regions'),
        ),
        migrations.AddField(
            model_name='filter',
            name='trustees',
            field=models.ManyToManyField(to='auction.Trustee', blank=True, related_name='_trustees_+', verbose_name='trustees'),
        ),
        migrations.AddField(
            model_name='filter',
            name='user',
            field=models.ForeignKey(null=True, to=settings.AUTH_USER_MODEL, verbose_name='user'),
        ),
        migrations.AddField(
            model_name='document',
            name='lot',
            field=models.ForeignKey(null=True, to='auction.Lot', verbose_name='lot'),
        ),
        migrations.AddField(
            model_name='debtor',
            name='region',
            field=models.ForeignKey(null=True, to='auction.Region', verbose_name='region'),
        ),
    ]
