from django.conf.urls import patterns, url
from auction.views import *

urlpatterns = patterns(
    '',
    url(r'^$', lot_list, name='lot_list'),
    url(r'^favorite/(?P<id>\d+)/$', favorite_list, name='favorite_list'),
    url(r'^favorite/$', favorite_list, name='favorite_list'),
    url(r'^(?P<pk>\d+)/$', lot_details, name='lot_details'),
    url(r'^mark_read/(?P<lot_id>\d+)/$', toggle_lot_read_ajax, name='mark_read'),
    url(r'^read_all/$', read_all, name='read_all'),
    url(r'^add_chapter$', add_chapter, name='add_chapter'),
    url(r'^del_chapter/(?P<id>\d+)$', del_chapter, name='del_chapter'),
    url(r'^get_comment/(?P<lot_id>\d+)/$', get_comment, name='get_comment'),
    url(r'^set_comment/(?P<lot_id>\d+)/$', set_comment, name='set_comment'),
    url(r'^add_to_chapter/$', add_to_chapter, name='add_to_chapter'),
    url(r'^remove_from_chapter/$', remove_from_chapter, name='remove_from_chapter'),
    url(r'^calendar/$', calendar, name='calendar'),
    url(r'^data_by_month/(?P<year>\d+)/(?P<month>\d+)/$', data_by_month, name='data_by_month'),
    url(r'^monitoring/$', monitoring, name='monitoring'),
    url(r'^monitoring/filter/$', filter_edit, name='filter_add'),
    url(r'^monitoring/filter/(?P<filter_id>\d+)/$', filter_edit, name='filter_edit'),
    url(r'^question/$', question_form, name='question_form'),

    # blog
    url(r'^blog/$', blog, name='blog'),
    url(r'^blog/blog_theme$', blog_theme, name='blog_theme'),
    url(r'^blog/blog_theme/blog_question$', blog_question, name='blog_question'),
)
