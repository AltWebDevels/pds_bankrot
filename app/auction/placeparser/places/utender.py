import re
import requests
from bs4 import BeautifulSoup

def parse_lot_number(lot_number):
    group = re.findall('^.*-(\d+)/(\d+)', lot_number)[0]
    return int(group[0]), int(group[1])

def get_url(lot_number):
    search_url = 'http://www.utender.ru'
    trade_number, lot_number = parse_lot_number(lot_number)
    session = requests.Session()
    request_body = {
        'ctl00$ctl00$MainExpandableArea$phExpandCollapse$scPurchaseAllSearch$vPurchaseLot_purchaseNumber_торга': trade_number,
        'ctl00$ctl00$MainExpandableArea$phExpandCollapse$scPurchaseAllSearch$vPurchaseLot_lotNumber_лота': lot_number,
    }
    response = session.post(search_url, data=request_body)
    soup = BeautifulSoup(response.content)
    rel_url = soup.find('a', id='ctl00_ctl00_MainContent_ContentPlaceHolderMiddle_srPurchaseAllSearch_ctl03_HyperLink3')['href']
    abs_path = '{}{}'.format(search_url, rel_url)
    return abs_path