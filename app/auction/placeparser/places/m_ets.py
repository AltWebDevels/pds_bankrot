import re
from urllib.request import quote
import requests
from bs4 import BeautifulSoup


def get_url(lot_number):
    search_url = 'http://www.m-ets.ru/search?r_num={}'
    response = requests.get(search_url.format(quote(lot_number)))
    soup = BeautifulSoup(response.content)
    bs_tr = soup.find('div', id='content').find('table', attrs={'class': 'tablesorter1'}).findAll('tr')[1]['onclick']
    trade_id = re.findall('id=(\d+)\'', str(bs_tr))[0]
    abs_path = 'http://www.m-ets.ru/generalView?id={}'.format(trade_id)
    return abs_path
