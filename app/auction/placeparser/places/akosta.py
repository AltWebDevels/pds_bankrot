import re
from urllib.request import quote
import requests
from bs4 import BeautifulSoup

def parse_lot_number(lot_number):
    group = re.findall('^(.+)-', lot_number)[0]
    return group

def get_url(lot_number):
    search_url = 'http://www.akosta.info/etp/trade/list.html?subject=&number={}'
    trade_number = parse_lot_number(lot_number)
    response = requests.get(search_url.format(quote(trade_number.encode('cp1251'))))
    soup = BeautifulSoup(response.content)
    bs_tr = soup.find('td', id='page').find('table', attrs={'class': 'data'}).findAll('tr')[1]
    trade_id = re.findall('id=(\d+)\'', str(bs_tr))[0]
    abs_path = 'http://www.akosta.info/trade/view/purchase/general.html?id={}'.format(trade_id)
    return abs_path