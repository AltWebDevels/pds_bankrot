import importlib

class Parser(object):
    def get_module_name(self, url):
        if 'utender' in url: return 'utender'
        if 'fabrikant' in url: return 'fabrikant'
        if 'akosta' in url: return 'akosta'
        if 'atctrade' in url: return 'atctrade'
        if 'b2b-center' in url: return 'b2b_center'
        if 'lot-online' in url: return 'lot_online'
        if 'm-ets' in url: return 'm_ets'
        raise KeyError

    lot_number = None
    parser = None
    module_absent = False
    def __init__(self, tradeplace_url, lot_number):
        self.lot_number = lot_number
        self.tradeplace_url = tradeplace_url
        try:
            self.module_name = self.get_module_name(tradeplace_url)
            try:
                module_path = 'auction.placeparser.places.{}'.format(self.module_name)
                self.parser_module = importlib.import_module(module_path)
            except ImportError:
                module_path = 'places.{}'.format(self.module_name)
                self.parser_module = importlib.import_module(module_path)
        except KeyError:
            self.module_absent = True
    def get_url(self):
        if self.module_absent:
            return self.tradeplace_url
        return self.parser_module.get_url(self.lot_number)

if __name__ == '__main__':
    import sys
    sys.path.append('/home/vagrant/share/app')
    parser = Parser('http://www.m-ets.ru/', '11388-ОАОФ')
    print(parser.get_url())