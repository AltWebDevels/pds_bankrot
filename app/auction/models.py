from django.db import models
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from django.db.models import Max
from django.conf import settings

class LotType(models.Model):
    title = models.CharField(max_length=200, null=True, unique=True, verbose_name=_('title'))
    code = models.CharField(max_length=200, null=True, verbose_name=_('code'))

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _('lot type')
        verbose_name_plural = _('lot types')


class Trustee(models.Model):
    vatin = models.CharField(max_length=200, null=True,
                             verbose_name=_('vatin'))
    name = models.CharField(max_length=200, null=True, verbose_name=_('name'))
    fed_url = models.URLField(verbose_name=_('fed url'), null=True,
                              unique=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('trustee')
        verbose_name_plural = _('trustees')


class LotMessage(models.Model):
    lot = models.ForeignKey('Lot', null=True, verbose_name=_('lot'))
    title = models.CharField(max_length=200, null=True,
                             verbose_name=_('title'))
    date = models.DateTimeField(verbose_name=_('date'), null=True)
    fed_url = models.URLField(verbose_name=_('source'), null=True, unique=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _('lot message')
        verbose_name_plural = _('lot messages')


class DebtorMessage(models.Model):
    debtor = models.ForeignKey('Debtor', null=True, verbose_name=_('debtor'))
    title = models.CharField(max_length=200, null=True,
                             verbose_name=_('title'))
    date = models.DateTimeField(verbose_name=_('date'), null=True)
    fed_url = models.URLField(verbose_name=_('source'), null=True, unique=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _('debtor message')
        verbose_name_plural = _('debtor messages')


class Region(models.Model):
    title = models.CharField(max_length=200, null=True,
                             verbose_name=_('title'))

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _('region')
        verbose_name_plural = _('regions')


class Debtor(models.Model):
    title = models.CharField(max_length=300, null=True,
                             verbose_name=_('title'), unique=True)
    region = models.ForeignKey('Region', null=True, verbose_name=_('region'))
    vatin = models.CharField(max_length=20, null=True, verbose_name=_('vatin'))
    register_id = models.CharField(max_length=200, null=True,
                                   verbose_name=_('register id'))
    address = models.CharField(max_length=500, null=True,
                               verbose_name=_('address'))
    fed_url = models.URLField(verbose_name=_('fed url'), null=True,
                              unique=True)

    class Meta:
        verbose_name = _('debtor')
        verbose_name_plural = _('debtors')

    def __str__(self):
        return self.title


class Organizer(models.Model):
    vatin = models.CharField(max_length=200, null=True,
                             verbose_name=_('vatin'))
    name = models.CharField(max_length=200, null=True, verbose_name=_('name'))
    fed_url = models.URLField(verbose_name=_('fed url'), null=True,
                              unique=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('organizer')
        verbose_name_plural = _('organizers')


class Marketplace(models.Model):
    title = models.CharField(max_length=200, null=True,
                             verbose_name=_('title'), unique=True)
    url = models.URLField(verbose_name=_('url'), null=True)
    img = models.ImageField(null=True, verbose_name=_('image'))
    fed_url = models.URLField(verbose_name=_('fed url'), null=True,
                              unique=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _('marketplace')
        verbose_name_plural = _('marketplaces')


class Currency(models.Model):
    title = models.CharField(max_length=200, null=True,
                             verbose_name=_('title'))
    sign = models.CharField(max_length=4, null=True, verbose_name=_('sign'))

    def __str__(self):
        return str(self.title)

    class Meta:
        verbose_name = _('currency')
        verbose_name_plural = _('currencies')


class Status(models.Model):
    title = models.CharField(max_length=200, null=True,
                             verbose_name=_('title'))

    def __str__(self):
        return str(self.title)

    class Meta:
        verbose_name = _('status')
        verbose_name_plural = _('statuses')


class UserData(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, verbose_name=_('user'))
    lot = models.ForeignKey('Lot', null=True, verbose_name=_('lot'))
    comment = models.TextField(null=True, verbose_name=_('comment'))
    read = models.BooleanField(default=False, verbose_name=_('read'))
    sent = models.BooleanField(default=False, verbose_name=_('sent'))
    chapter = models.ForeignKey('Chapter', null=True, verbose_name=_('chapter'))

    def __str__(self):
        return '{} {}'.format(self.user, self.lot)

    class Meta:
        verbose_name = _('user data')
        verbose_name_plural = _('user data')


class Chapter(models.Model):
    title = models.CharField(max_length=200, null=True,
                             verbose_name=_('title'))
    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, verbose_name=_('user'))

    def __str__(self):
        return self.title

    def get_lot_count(self, user_id):
        return Lot.objects.filter(users_data=user_id,
                                  userdata__chapter_id=self.id).count()

    class Meta:
        verbose_name = _('chapter')
        verbose_name_plural = _('chapters')


class Lot(models.Model):
    active = models.BooleanField(default=False, verbose_name=_('active'))
    number = models.CharField(max_length=200, verbose_name=_('number'),
                              null=True, unique=True)
    marketplace = models.ForeignKey(Marketplace, null=True, verbose_name=_('marketplace'))
    fed_url = models.URLField(verbose_name=_('fed url'), null=True,
                              unique=True)
    marketplace_url = models.URLField(verbose_name=_('marketplace url'),
                                      null=True)
    description = models.TextField(verbose_name=_('description'), null=True)
    currency = models.ForeignKey(Currency, null=True, verbose_name=_('currency'))
    status = models.ForeignKey(Status, null=True)
    date_message = models.DateTimeField(verbose_name=_('placement date'),
                                        null=True)
    date_start = models.DateTimeField(verbose_name=_('trading start date'),
                                      null=True)
    date_finish = models.DateTimeField(verbose_name=_('trading finish date'),
                                       null=True)
    date_app_from = models.DateTimeField(null=True, verbose_name=_('apply date from'))
    date_app_to = models.DateTimeField(null=True, verbose_name=_('apply date to'))
    case_number = models.CharField(max_length=200, null=True, verbose_name=_('case number'))
    cort = models.CharField(max_length=200, null=True, verbose_name=_('cort'))
    debtor = models.ForeignKey(Debtor, null=True, verbose_name=_('debtor'))
    trustee = models.ForeignKey(Trustee, null=True, verbose_name=_('trustee'))
    organizer = models.ForeignKey(Organizer, null=True, verbose_name=_('organizer'))
    lot_type = models.ForeignKey(LotType, null=True, verbose_name=_('lot type'))
    price_start = models.DecimalField(max_digits=15, decimal_places=2, null=True, verbose_name=_('price start'))
    users_data = models.ManyToManyField(settings.AUTH_USER_MODEL, through='UserData', blank=True,
                                        related_name='+', verbose_name=_('user data'))

    class Meta:
        verbose_name = _('lot')
        verbose_name_plural = _('lots')

    def __str__(self):
        return self.number

    def get_absolute_url(self):
        return reverse('auction:lot_details', kwargs={'pk': self.id})

    def get_price(self):
        max_date = self.price_set.aggregate(Max('date'))['date__max']
        return self.price_set.filter(date=max_date)[0]


class Price(models.Model):
    lot = models.ForeignKey(Lot, null=True, related_name='price_set', verbose_name=_('lot'))
    value = models.DecimalField(max_digits=15, decimal_places=2, null=True, verbose_name=_('value'))
    date = models.DateTimeField(null=True, verbose_name=_('date'))

    def __str__(self):
        return str(self.value)

    class Meta:
        verbose_name = _('price')
        verbose_name_plural = _('prices')


class Document(models.Model):
    lot = models.ForeignKey(Lot, null=True, verbose_name=_('lot'))
    title = models.CharField(max_length=200, null=True,
                             verbose_name=_('title'))
    fed_url = models.URLField(verbose_name=_('url'), null=True, unique=True)
    date = models.DateTimeField(verbose_name=_('date'), null=True)

    class Meta:
        verbose_name = _('document')
        verbose_name_plural = _('documents')


class Filter(models.Model):
    def __str__(self):
        return self.title

    PERIODS = (
        (1, _('hourly')),
        (2, _('daily')),
        (3, _('weekly')),
    )
    is_active = models.BooleanField(default=False, verbose_name=_('active'))
    title = models.CharField(max_length=200, null=True, blank=False,
                             verbose_name=_('title'))
    key_words = models.TextField(max_length=100, blank=True,
                                 verbose_name=_('keywords'), null=True)
    is_all_keywords_necessary = models.BooleanField(default=False, blank=True, verbose_name=_('all keywords necessary'))
    price_from = models.FloatField(null=True, blank=True, verbose_name=_('price from'))
    price_to = models.FloatField(null=True, blank=True, verbose_name=_('price to'))
    lot_types = models.ManyToManyField('LotType', blank=True, related_name='+', verbose_name=_('lot types'))
    regions = models.ManyToManyField('Region', blank=True, related_name='+', verbose_name=_('regions'))
    debtors = models.ManyToManyField('Debtor', blank=True, related_name='+', verbose_name=_('debtors'))
    trustees = models.ManyToManyField('Trustee', blank=True, related_name='+', verbose_name=_('trustees'))
    email_info = models.BooleanField(default=False, verbose_name=_('email info'))
    email_period = models.IntegerField(choices=PERIODS, null=True, blank=True, verbose_name=_('email period'))
    sms_info = models.BooleanField(default=False, verbose_name=_('sms info'))
    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, verbose_name=_('user'))

    class Meta:
        verbose_name = _('filter')
        verbose_name_plural = _('filters')


class Question(models.Model):
    name = models.CharField(max_length=100, null=True, blank=False,
                            verbose_name=_('name'))
    email = models.EmailField(max_length=100, null=True, blank=False,
                              verbose_name=_('email'))
    phone = models.CharField(max_length=100, null=True, blank=False,
                             verbose_name=_('phone'))
    message = models.TextField(max_length=100, blank=True,
                               verbose_name=_('message'), null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('question')
        verbose_name_plural = _('questions')
