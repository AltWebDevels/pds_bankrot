'use strict';
var date = new Date();

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

var getCurrentMonth = function(month) {
  var months = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь',
   'Октябрь', 'Ноябрь', 'Декабрь'];
  // var month = date.getMonth();

  return months[month];
};

var getCountOfDaysInMonth = function(year, month){
  return new Date(year, month + 1, 0).getDate();
};

var makeTable = function(){
  var list = document.getElementById('list');

  for(var i = 0; i < 6; i++) {
    var tr = document.createElement('tr');
    list.appendChild(tr);
    for(var j = 0; j < 7; j++){
      var td = document.createElement('td');
      tr.appendChild(td);
    }
  }
};

var Month = {
  year: date.getFullYear(),
  month: date.getMonth(),
  currentMonth: function(){
    return getCurrentMonth(this.month);
  },
  firstDay: function(){
    return new Date(this.year, this.month, 0);
  },
  day: function() {
    var a = this.firstDay();
    return a.getDay();
  },
  days: function(){
    return getCountOfDaysInMonth(this.year, this.month);
  },
  count: function(){
    var tds = document.querySelectorAll('#list td');
    var a = this.days();
    var b = this.day();
    for(var i = 1; i < a+1; i++){
      tds[b].innerHTML = '<span>' + i + '</span>';
      b++;
    }
  },
  nextMonth: function(){
    this.month += 1;
    if (this.month > 11) {
    	this.month = 0;
    	this.year += 1;

    }
    return this.month, this.year;
  },
  prevMonth: function(){
    this.month -= 1;
    if (this.month < 0) {
    	this.month = 11;
    	this.year -= 1;
    }
    return this.month, this.year;
  },
  make: function(){
    $('#list').empty();
    makeTable();
    Month.count();
    Month.loadEvents();
    $('.current-year').html(Month.year);
    $('.current-month').html(Month.currentMonth());
  },
  set_comment: function(lot_id, newComment){
    $.ajax
    ({
        type: 'POST',
        url: '/auction/set_comment/'+ lot_id + '/',
        data: {
            comment: newComment,
            csrfmiddlewaretoken: getCookie('csrftoken')
        },
        dataType: 'json',
        success: function () {
            console.log("Thanks!");
        }
    });
  },
  loadEvents: function() {

    var url = '/auction/data_by_month/' + this.year + '/' + (this.month + 1);
    $.getJSON(url, function(data) {
      $.each(data, function( key, val ) {
        var tds = $('#list').find('td');
        for (var item in val) {

          $.each(tds, function(index, value){
            var span = $(this).find('span');

            if(span.html() === key) {

              var arr = val[item].number.split('');

              for(var i = 0; i < arr.length; i++){
                if (arr[i] === "/"){
                  arr.splice(i, 1);
                  arr = arr.join('');
                  val[item].number = arr;
                }
              }

              if (val[item].comment === null){
                val[item].comment = "Комментарий отсутствует";
              }

              $(this).append($('<div>', {
                'class': 'tableEvent',
                html: '<button type="button" class="btn btn-primary" data-toggle="modal" \
                        data-target=".' + val[item].number + '">' + val[item].number +
                      '</button>' +

                      '<div class="modal fade ' + val[item].number + ' bs-example-modal-sm" tabindex="-1" \
                      role="dialog" aria-labelledby="mySmallModalLabel">' +

                        '<div class="modal-dialog modal-sm">' +

                          '<div class="modal-content">' +

                            '<div class="modal-header">' +

                              '<button type="button" class="close" data-dismiss="modal" \
                              aria-label="Close"><span aria-hidden="true">' + '&times;' + '</span></button>' +

                              '<h4 class="modal-title" id="myModalLabel">' + '</h4>' +

                            '</div>' +

                            '<div class="modal-body">' +

                              '<div class="tableEvent__lotUrl">' + 'Ссылка: ' + '<a href="' + val[item].url + '">' + 'Лот № ' + val[item].number +
                              '</a>' + '</div>' +

                              '<div class="tableEvent__comment">' + 'Ваш комментарий: ' + val[item].comment + '</div>' +

                              //'<div class="modal-footer">' +
                              //  '<input type="submit" class="btn btn-primary set_comment" value="Ok"/>' +
                              //  '<button type="button" class="btn btn-default" data-dismiss="modal">' + 'Отмена' + '</button>' +
                              //'</div>' +

                            '</div>' +

                          '</div>' +
                        '</div>' +
                      '</div>'

              }));
              var btn = $(this).find('.set_comment');
              var newComment = $('.comment-text-modal').html();
              console.log(newComment);
              btn.on('click', function(){
                Month.set_comment(val[item].lot_id, newComment);
                console.log(newComment);
              });
            }
          });
        }
      });
    });
  }
};

$(function(){
  Month.make();

  $('.nextMonth').on('click', function(){
    Month.nextMonth();
    Month.make();
  });

  $('.prevMonth').on('click', function(){
    Month.prevMonth();
    Month.make();
  });


});

