from django.core.management.base import BaseCommand, CommandError
from auction.tasks import validate_lots

class Command(BaseCommand):
    help = 'Fedresurs parsing'

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        validate_lots.delay()
