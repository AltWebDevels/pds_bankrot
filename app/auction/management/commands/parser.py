from django.core.management.base import BaseCommand, CommandError
from auction import tasks


class Command(BaseCommand):
    help = 'Fedresurs parsing'

    def add_arguments(self, parser):
        parser.add_argument('method', nargs='+', type=str)

    def handle(self, *args, **options):
        self.stdout.write('Run {} ...'.format(options['method']))

        if options['method'][0] == 'update_lots':
            tasks.update_lots.delay()
            self.stdout.write('Task for updating inactive lots added in queue')

        if options['method'][0] == 'update_debtors':
            tasks.update_debtors.delay()
            self.stdout.write('Task for updating debtors added in queue')

        if options['method'][0] == 'update_all_lots':
            tasks.update_lots.delay(with_active=True)
            self.stdout.write('Task for updating inactive lots added in queue')

        if options['method'][0] == 'parse_all':
            tasks.parse_all.delay()
            self.stdout.write('Parsing all lots from fedresurs added in queue')

        if options['method'][0] == 'parse_index':
            tasks.process_pages.delay()
            self.stdout.write('Parsing index page fedresurs added in queue')
