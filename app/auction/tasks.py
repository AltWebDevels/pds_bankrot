from __future__ import absolute_import

from celery import shared_task
from celery.schedules import crontab
from celery.decorators import periodic_task
from django.db.models import Count, Q
from django.conf import settings
from django.core.mail import send_mail
from functools import reduce
from operator import __or__ as OR
import requests
from bs4 import BeautifulSoup
import time
import re
from datetime import datetime
import urllib
from celery.utils.log import get_task_logger
from auction.placeparser import parser
from auction.models import *
from django.template.loader import render_to_string

logger = get_task_logger(__name__)


def get_request_url():
    return 'http://bankrot.fedresurs.ru/TradeList.aspx'


def get_request_headers():
    return {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36',
        'Accept': '*/*',
        'Accept-Encoding': 'gzip,deflate,sdch',
        'Accept-Language': 'en-US,en;q=0.8',
    }


def get_request_body():
    return {
        '__EVENTTARGET': None,
        '__EVENTARGUMENT': None,
        '__VIEWSTATE': None,
        '__EVENTVALIDATION': None,
        'ctl00$cphBody$ucTradeStatus$ddlBoundList': None,
        'ctl00$cphBody$ucRegion$ddlBoundList': None,
        'ctl00$cphBody$ucTradeType$ddlBoundList': None,
    }


def get_cookie_dict():
    return {
        'Region': '',
        'Status': '',
        'Type': '',
        'TradeObject': '',
        'TradeCode': '',
        'TradePlaceId': '',
        'DebtorText': '',
        'DebtorId': '',
        'DebtorType': '',
        'PropertyCategoriesText': '',
        'PropertyCategoriesValue': '',
        'PropertyCategoriesType': '',
        'ArmTOText': '',
        'ArmTOType': '',
        'ArmTOId': '',
        'PageNumber': 0,
        'DateEndValue': '',
        'DateBeginValue': '',
        'SearchForDocuments': False,
    }


@periodic_task(run_every=(crontab(minute=0)))
def parse_index():
    process_pages()


@shared_task
def parse_all():
    request_url = get_request_url()
    request_headers = get_request_headers()
    session = requests.Session()
    response = session.get(request_url, headers=request_headers)
    soup = BeautifulSoup(response.content)
    regions = sorted([int(option.get('value')) for option in
                      soup.find('select', id='ctl00_cphBody_ucRegion_ddlBoundList').find_all('option') if
                      option.get('value')])
    types = sorted([int(option.get('value')) for option in
                    soup.find('select', id='ctl00_cphBody_ucTradeType_ddlBoundList').find_all('option') if
                    option.get('value')])
    statuses = sorted([int(option.get('value')) for option in
                       soup.find('select', id='ctl00_cphBody_ucTradeStatus_ddlBoundList').find_all('option')
                       if option.get('value')])
    for region in regions:
        reg_full = is_overflow(session, response, region=region)
        if not reg_full:
            if reg_full is None: continue
            logger.info(process_pages.delay(region=region))
            continue
        for type in types:
            type_full = is_overflow(session, response, region=region, type=type)
            if not type_full:
                if type_full is None: continue
                logger.info(process_pages.delay(region=region, type=type))
                continue
            for status in statuses:
                logger.info(process_pages.delay(region=region, type=type, status=status))


def is_overflow(session, response, region=None, type=None, status=None):
    request_url = get_request_url()
    request_body = get_request_body()
    request_headers = get_request_headers()
    logger.info('Region: {0}, Type: {1}, Status: {2}'.format(region, type, status))
    request_body['ctl00$cphBody$ucRegion$ddlBoundList'] = region
    request_body['ctl00$cphBody$ucTradeType$ddlBoundList'] = type
    request_body['ctl00$cphBody$ucTradeStatus$ddlBoundList'] = status
    soup = BeautifulSoup(response.content)
    request_body['__VIEWSTATE'] = soup.select('input[name=__VIEWSTATE]')[0]['value']
    request_body['__EVENTVALIDATION'] = soup.select('input[name=__EVENTVALIDATION]')[0]['value']
    time.sleep(2)
    response = session.post(request_url, data=request_body, headers=request_headers)
    soup = BeautifulSoup(response.content)
    if 'По заданным критериям не найдено ни одной записи' in response.text:
        return None
    try:
        count_string = soup.find('td', id='ctl00_cphBody_PaggingAdvInfo1_tdPaggingAdvInfo').text
    except:
        return True
        # todo: to logs
    count = int(re.findall('(\d+)\)', count_string)[0])
    if count <= 20:
        parse_page(response.content)
        return None
    logger.info('Count: {}'.format(re.findall('(\d+)\)', count_string)[0]))
    if count == 1000:
        return True
    return False


@shared_task
def process_pages(region=None, type=None, status=None):
    request_url = get_request_url()
    request_body = get_request_body()
    request_headers = get_request_headers()
    cookie_dict = get_cookie_dict()
    request_body['ctl00$cphBody$ucRegion$ddlBoundList'] = cookie_dict['Region'] = region
    request_body['ctl00$cphBody$ucTradeType$ddlBoundList'] = cookie_dict['Type'] = type
    request_body['ctl00$cphBody$ucTradeStatus$ddlBoundList'] = cookie_dict['Status'] = status
    session = requests.Session()
    response = session.get(request_url, headers=request_headers)

    soup = BeautifulSoup(response.content)
    request_body['__VIEWSTATE'] = soup.select('input[name=__VIEWSTATE]')[0]['value']
    request_body['__EVENTVALIDATION'] = soup.select('input[name=__EVENTVALIDATION]')[0]['value']

    logger.info('Region: {0}, Type: {1}, Status: {2}'.format(request_body['ctl00$cphBody$ucRegion$ddlBoundList'],
                                                             request_body['ctl00$cphBody$ucTradeType$ddlBoundList'],
                                                             request_body['ctl00$cphBody$ucTradeStatus$ddlBoundList']))
    time.sleep(2)
    response = session.post(request_url, data=request_body, headers=request_headers)
    if 'По заданным критериям не найдено ни одной записи.' in response.text:
        return False
    soup = BeautifulSoup(response.content)
    records = int(
        re.findall('(\d+)\)', soup.find('td', id='ctl00_cphBody_PaggingAdvInfo1_tdPaggingAdvInfo').text)[0])
    pages = records // 20 + 1 if records % 10 else records // 20
    page = 1
    logger.info('Page: {}'.format(page))
    parse_page(response.content)
    page += 1

    request_body['__EVENTTARGET'] = 'ctl00$cphBody$gvTradeList'
    while page <= pages:
        request_body['__EVENTARGUMENT'] = 'Page${0}'.format(page)
        request_body['__VIEWSTATE'] = soup.select('input[name=__VIEWSTATE]')[0]['value']
        request_body['__EVENTVALIDATION'] = soup.select('input[name=__EVENTVALIDATION]')[0]['value']
        session.cookies._cookies['bankrot.fedresurs.ru']['/']['Trade'].value = urllib.parse.urlencode(cookie_dict)
        logger.info('Page: {}'.format(page))

        time.sleep(2)
        response = session.post(request_url, data=request_body, headers=request_headers)
        soup = BeautifulSoup(response.content)
        parse_page(response.content)
        page += 1

    return True


def parse_page(content):
    soup = BeautifulSoup(content)
    trade_rows = soup.find('table', id='ctl00_cphBody_gvTradeList').find_all('tr')[1:-2]
    for trade in trade_rows:
        columns = trade.find_all('td')

        trade_data = {
            'number': columns[0].get_text().strip(),
            'trade_card_url': 'http://bankrot.fedresurs.ru{}'.format(columns[5].find('a')['href']),
            'trade_place_card_url': 'http://bankrot.fedresurs.ru{}'.format(columns[3].find('a')['href']),
            'trade_place': columns[3].get_text().strip(),
            'type': columns[5].get_text().strip(),
            'status': columns[6].get_text().strip(),
        }
        marketplace, created = Marketplace.objects.get_or_create(title=trade_data['trade_place'])
        if created:
            marketplace.fed_url = trade_data['trade_place_card_url']
            response = requests.get(marketplace.fed_url)
            marketplace.url = BeautifulSoup(response.content).find('a', id='ctl00_cphBody_aTradeSite')['href']
            marketplace.save()
        lot, created = Lot.objects.get_or_create(number=trade_data['number'])
        if created or lot.marketplace_url == marketplace.url or lot.marketplace_url == marketplace.fed_url:
            parser_object = parser.Parser(marketplace.url, trade_data['number'])
            lot.marketplace_url = parser_object.get_url()
        lot.status, created = Status.objects.get_or_create(title=trade_data['status'])
        lot.fed_url = trade_data['trade_card_url']
        lot.lot_type, created = LotType.objects.get_or_create(title=trade_data['type'])
        lot.marketplace = marketplace
        lot.save()
        fill_lot.delay(lot.id)
    return len(trade_rows)


@shared_task
def set_tradeplace_url(lot_id):
    return True
    # lot = Lot.objects.get(pk=lot_id)
    # todo: fill this after tradeplace parsers


@shared_task
def fill_lot(lot_id):
    request_headers = get_request_headers()
    session = requests.Session()
    try:
        lot = Lot.objects.get(pk=lot_id)
        res = session.get(lot.fed_url)
    except:
        return False

    bs = BeautifulSoup(res.content)
    # description
    try:
        str_with_url = bs.find('a', id='ctl00_cphBody_rptLotList_ctl00_HyperLink1')['onclick']
        desc_url = re.findall('\(\'(.*)\', \'С', str_with_url)[0]
        res_tradecard = requests.get('http://bankrot.fedresurs.ru{0}'.format(desc_url))
        description = BeautifulSoup(res_tradecard.content).find_all('tr')[1].text.strip()
    except:
        description = bs.find('tr', id='ctl00_cphBody_rptLotList_ctl00_Tr5').find('div').text.strip()
    lot.description = description
    # lot.title = description.split('.')[0][:100] + '...'

    # currency
    currency, created = Currency.objects.get_or_create(title='рублей', sign='rub')  # fixme
    if created:
        currency.save()
    lot.currency = currency
    find_part = bs.find('tr', id='ctl00_cphBody_dateObtainingMessage')

    # date placement
    lot.date_message = datetime.strptime(find_part.find('b').text.strip(), '%d.%m.%Y %H:%M')
    # start price
    try:
        lot.price_start = float(bs.find('tr', id='ctl00_cphBody_rptLotList_ctl00_trStartPrice').find('td', attrs={
            'class': 'StaticText'}).text.strip().replace(',', '.').replace(' ', ''))
    except AttributeError:
        lot.price_start = None

    # price
    Price.objects.get_or_create(
        value=lot.price_start,
        lot_id=lot.id,
        date=lot.date_message
    )

    # date add from
    lot.date_app_from = datetime.strptime(bs.find('tr', id='ctl00_cphBody_trTradeBegnDate').find('b').text.strip(),
                                          '%d.%m.%Y %H:%M')
    # date app to
    lot.date_app_to = datetime.strptime(bs.find('tr', id='ctl00_cphBody_trTradeEndDate').find('b').text.strip(),
                                        '%d.%m.%Y %H:%M')

    # dates start and finish
    try:
        lot.date_start = datetime.strptime(bs.find('tr', id='ctl00_cphBody_dateBegin').find('b').text.strip(),
                                           '%d.%m.%Y %H:%M')
        lot.date_finish = datetime.strptime(bs.find('tr', id='ctl00_cphBody_dateFinish').find('b').text.strip(),
                                            '%d.%m.%Y %H:%M')
    except:
        lot.date_start = lot.date_app_from
        lot.date_finish = lot.date_app_to

    # debtor
    try:
        debtor, created = Debtor.objects.get_or_create(
            title=bs.find('a', id='ctl00_cphBody_lnkDebtor').text.strip())
        if created:
            debtor.fed_url = 'http://bankrot.fedresurs.ru{0}'.format(
                bs.find('a', id='ctl00_cphBody_lnkDebtor')['href'])
            debtor.save()
            fill_debtor.delay(debtor.id)
    except:
        debtor_name = bs.find('tr', id='ctl00_cphBody_trDebtor').find('b').text.strip()
        debtors = Debtor.objects.filter(title=debtor_name)
        if debtors:
            debtor = debtors[0]
        else:
            debtor, created = Debtor.objects.get_or_create(title=debtor_name)
    lot.debtor = debtor

    # trustee
    try:
        trustee, created = Trustee.objects.get_or_create(
            fed_url='http://bankrot.fedresurs.ru{0}'.format(bs.find('a', id='ctl00_cphBody_lnkArbitrManager')['href'])
        )
        if created:
            trustee.name = bs.find('a', id='ctl00_cphBody_lnkArbitrManager').text.strip()
            trustee.save()
            fill_trustee.delay(trustee.id)
    except:
        trustee_name = bs.find('tr', id='ctl00_cphBody_trArbitrManager').find('b').text.strip()
        trusties = Trustee.objects.filter(name=trustee_name)
        if trusties:
            trustee = trusties[0]
        else:
            trustee, created = Trustee.objects.get_or_create(name=trustee_name)
    lot.trustee = trustee

    # organizer
    try:
        organizer, created = Organizer.objects.get_or_create(
            fed_url='http://bankrot.fedresurs.ru{0}'.format(bs.find('a', id='ctl00_cphBody_lnkTradeOrganizer')['href'])
        )
        if created:
            organizer.name = bs.find('a', id='ctl00_cphBody_lnkTradeOrganizer').text.strip()
            organizer.save()
            fill_organizer.delay(organizer.id)
    except:
        if bs.find(id='ctl00_cphBody_trTradeOrganizer'):
            organizer_name = bs.find('tr', id='ctl00_cphBody_trTradeOrganizer').find('b').text.strip()
            organizers = Organizer.objects.filter(name=organizer_name)
            if organizers:
                organizer = organizers[0]
            else:
                organizer, created = Organizer.objects.get_or_create(name=organizer_name)
        else:
            organizer = None
    lot.organizer = organizer
    # Tabs
    tabs_count = len(bs.find_all('li', attrs={'class': 'rtsLI'}))
    if tabs_count == 3:
        tab_messages_index = 1
        tab_documents_index = None
        tab_advanced_index = 2
    else:
        tab_messages_index = 1
        tab_documents_index = 2
        tab_advanced_index = 3

    request_headers['X-MicrosoftAjax'] = 'Delta=true'
    # advanced
    request_body = {
        'ctl00$PrivateOffice1$ctl00': 'ctl00$cphBody$UpdatePanel4|ctl00$cphBody$rtsTrade',
        '__EVENTTARGET': 'ctl00$cphBody$rtsTrade',
        '__EVENTARGUMENT': '{{"type":0,"index":"{0}"}}'.format(tab_advanced_index),
        '__VIEWSTATE': bs.select('input[name=__VIEWSTATE]')[0]['value']
    }
    res = session.post(lot.fed_url, data=request_body, headers=request_headers)
    bs = BeautifulSoup(res.content)
    lot.case_number = bs.find('tr', id='ctl00_cphBody_trSpecCaseNumber').find('b').text.strip()
    lot.cort = bs.find('tr', id='ctl00_cphBody_trSpecCourt').find('b').text.strip()
    # messages
    request_body['__EVENTARGUMENT'] = '{{"type":0,"index":"{0}"}}'.format(tab_messages_index)
    request_body['ctl00$cphBody$hfRequestID'] = re.findall('=(\d+)$', lot.fed_url)[0]
    res = session.post(lot.fed_url, data=request_body, headers=request_headers)
    bs = BeautifulSoup(res.content)
    if bs.find('tr', attrs={'class': 'pager'}):
        rows = bs.find('table', id='ctl00_cphBody_gvMessages').find_all('tr')[1:-2]
        # todo: add parsing all pages
    else:
        rows = bs.find('table', id='ctl00_cphBody_gvMessages').find_all('tr')[1:]

    for row in rows:
        title = row.find('a').text.strip()
        date = datetime.strptime(row.td.text.strip(), '%d.%m.%Y %H:%M')
        fed_url = 'http://bankrot.fedresurs.ru{0}'.format(re.findall('Win\(\'(.*)\',\'\'', row.a['onclick'])[0])
        lot_message, created = LotMessage.objects.get_or_create(fed_url=fed_url)
        if created:
            lot_message.lot_id = lot_id
            lot_message.title = title
            lot_message.date = date
            lot_message.save()

    # documents
    lot.active = check_active(lot)
    if not tab_documents_index:
        lot.save()
        return lot.id

    request_body['__EVENTARGUMENT'] = '{{"type":0,"index":"{0}"}}'.format(tab_documents_index)
    res = session.post(lot.fed_url, data=request_body, headers=request_headers)
    bs = BeautifulSoup(res.content)
    rows = bs.find('table', id='ctl00_cphBody_gvDocuments').find_all('tr')[1:]
    for row in rows:
        title = row.find('a').text.strip()
        date = datetime.strptime(row.td.text.strip(), '%d.%m.%Y %H:%M:%S')
        fed_url = 'http://bankrot.fedresurs.ru{0}'.format(row.a['href'])
        Document.objects.get_or_create(lot_id=lot.id, title=title, date=date, fed_url=fed_url)
    return lot.save()


@shared_task
def fill_organizer(organizer_id):
    organizer = Organizer.objects.get(pk=organizer_id)
    if not organizer.fed_url or organizer.vatin:
        return False
    response = requests.get(organizer.fed_url)
    soup = BeautifulSoup(response.content)
    try:
        organizer.vatin = soup.find('tr', id='ctl00_cphBody_trINN').find('b').text.strip()
    except:
        organizer.vatin = soup.find('tr', id='ctl00_cphBody_trInn').find('b').text.strip()
    organizer.save()
    return True


@shared_task
def fill_trustee(trustee_id):
    trustee = Trustee.objects.get(pk=trustee_id)
    if not trustee.fed_url or trustee.vatin:
        return False
    response = requests.get(trustee.fed_url)
    soup = BeautifulSoup(response.content)
    trustee.vatin = soup.find('tr', id='ctl00_cphBody_trInn').find('b').text.strip()
    trustee.save()
    return True


@shared_task
def fill_debtor(debtor_id):
    debtor = Debtor.objects.get(pk=debtor_id)
    response = requests.get(debtor.fed_url)
    bs = BeautifulSoup(response.content)
    debtor.vatin = bs.find('tr', id='ctl00_cphBody_trINN').find('b').text.strip()
    debtor.region, created = Region.objects.get_or_create(
        title=bs.find('tr', id='ctl00_cphBody_trRegion').find('b').text.strip())
    debtor.register_id = bs.find('tr', id='ctl00_cphBody_trOGRN').find('b').text.strip()
    debtor.address = bs.find('tr', id='ctl00_cphBody_trAddress').find('b').text.strip()

    # messages
    if bs.find('table', id='ctl00_cphBody_gvMessages').find('tr', attrs={'class': 'pager'}):
        rows = bs.find('table', id='ctl00_cphBody_gvMessages').find_all('tr')[1:-2]
        # todo: add parsing all pages
    else:
        rows = bs.find('table', id='ctl00_cphBody_gvMessages').find_all('tr')[1:]

    for row in rows:
        title = row.find('a').text.strip()
        date = datetime.strptime(row.td.text.strip(), '%d.%m.%Y %H:%M:%S')
        fed_url = 'http://bankrot.fedresurs.ru{0}'.format(re.findall('Win\(\'(.*)\', \'С', row.a['onclick'])[0])
        DebtorMessage.objects.get_or_create(debtor_id=debtor.id, title=title, date=date, fed_url=fed_url)

    debtor.save()
    return True


@shared_task
def validate_lots():
    for lot in Lot.objects.all():
        if check_active(lot):
            lot.active = True
        else:
            lot.active = False
        lot.save()
    return True


def check_active(lot):
    for field in [
        lot.description,
        lot.currency,
        lot.date_start,
        lot.date_finish,
        lot.cort,
        lot.number,
        lot.marketplace_url,
        lot.debtor,
        lot.fed_url,
        lot.status,
        lot.price_start,
    ]:
        if not field:
            return False
    return True


@shared_task
def update_lots(with_active=False):
    if with_active:
        lots = Lot.objects.all()
    else:
        lots = Lot.objects.filter(active=False)

    for lot in lots:
        if not lot.fed_url:
            lot.delete()
            continue
        fill_lot.delay(lot.id)
    return True


@shared_task
def update_debtors():
    for debtor in Debtor.objects.all():
        if not debtor.fed_url:
            debtor.delete()
            continue
        fill_debtor.delay(debtor.id)
    return True


# filter tasks

def apply_filter(filter):
    lot_objects = Lot.objects.filter(active=True, date_finish__gte=datetime.now()).exclude(users_data=filter.user.id,
                                                                                           userdata__sent=True)
    if filter.key_words:
        if filter.is_all_keywords_necessary:
            lot_objects = lot_objects.filter(description__icontains=filter.key_words)
        else:
            # new_lot_objects = Lot.objects.none()
            q_objects = []
            for key_word in filter.key_words.split(' '):
                q_objects.append(Q(description__icontains=key_word))
            # for lot in lot_objects.filter(description__icontains=key_word):
            #         new_lot_objects.add(lot)
            # lot_objects = new_lot_objects
            lot_objects = lot_objects.filter(reduce(OR, q_objects))
    if filter.price_from:
        lot_objects = lot_objects.filter(price_set__value__gte=filter.price_from)
    if filter.price_to:
        lot_objects = lot_objects.filter(price_set__value__lte=filter.price_to)
    if filter.lot_types.count():
        lot_objects = lot_objects.filter(lot_type__in=filter.lot_types.all())
    if filter.regions.count():
        lot_objects = lot_objects.filter(debtor__region__in=filter.regions.all())
    if filter.debtors.count():
        lot_objects = lot_objects.filter(debtor__in=filter.debtors.all())
    if filter.trustees.count():
        lot_objects = lot_objects.filter(trustee__in=filter.trustees.all())

    return lot_objects.all()[:100]


@shared_task
def send_email_task(subject, message, email_from, emails_to, fail_silently=False, html_message=None):
    send_mail(subject, message, email_from, emails_to, fail_silently=fail_silently, html_message=html_message)


def process_filters(period=1):
    users = User.objects.raw('''
      SELECT DISTINCT auction_filter.user_id AS id
      FROM auction_filter JOIN auth_user ON auth_user.id = auction_filter.user_id
      AND auction_filter.is_active = TRUE
      AND auction_filter.email_period = %s''', [period])
    logger.info(users)
    for user in users:
        filters = Filter.objects.filter(is_active=True, user=user, email_period=period)
        lots = []
        logger.info(filters)
        for filter in filters:
            if not filter.is_active or not filter.email_info:
                continue
            lots += apply_filter(filter)

        logger.info(lots)
        if not lots:
            continue
        email_context = {
            'lots': set(lots),
            'user': user,
            'filters': filters
        }
        html_content = render_to_string('auction/email.html', email_context)
        plain_content = render_to_string('auction/email_plain.html', email_context)
        send_email_task.delay(
            settings.EMAIL_SUBJECT,
            plain_content,
            settings.EMAIL_HOST_USER,
            [user.email, ],
            html_message=html_content,
            fail_silently=False
        )
        logger.info('Email has been sent to {}'.format(user.email))
        for lot in lots:
            user_data, created = UserData.objects.get_or_create(user=user, lot=lot)
            user_data.sent = True
            user_data.save()


@periodic_task(run_every=(crontab(minute=30)))
def hourly_send():
    process_filters(period=1)


@periodic_task(run_every=(crontab(minute=30, hour=0)))
def hourly_send():
    process_filters(period=2)


@periodic_task(run_every=(crontab(0, 0, day_of_month='7, 14, 21, 28')))
def hourly_send():
    process_filters(period=3)


def main():
    process_filters()


if __name__ == '__main__':
    from django.core.wsgi import get_wsgi_application

    application = get_wsgi_application()
    main()
