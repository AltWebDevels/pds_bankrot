from django.test import TestCase, Client
from auction.models import Lot, UserData, Chapter
from django.core.urlresolvers import reverse


class GuestTestCase(TestCase):
    def setUp(self):
        self.client = Client()

    def test_guest_can_get_search_results(self):
        get_data = {
            'search_form': '1',
            'search': '123',
            'region': 'wer',
            'places': ('1',),
            'price1': '3',
            'price2': '321',
            'date1': '12/02/2015',
            'date2': '13/02/2015',
            'types': ('1',),
            'debtor': 'qwerty',
            'trustee': 'qwerty',
        }
        response = self.client.get(reverse('auction:lot_list'), get_data, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_guest_sees_all_open_lots(self):
        pass

    def test_guest_can_filter_lots_with_form(self):
        pass

    def test_guest_doesnt_see_user_spec_features(self):
        pass

    def test_guest_doesnt_see_hidden_strings(self):
        pass


class UserTestCase(TestCase):
    fixtures = ['test_data', ]
    username = 'vpupkin@gmail.com'
    password = 'pass1'

    def setUp(self):
        self.client = Client()
        self.client.login(username=self.username, password=self.password)

    def test_calendar_data(self):
        '''
        Test ajax method returns all data
        '''
        for i in range(3):
            UserData.objects.create(user_id=self.client.session['_auth_user_id'],
                                    lot=Lot.objects.create(date_app_to='2015-12-08'),
                                    chapter=Chapter.objects.create(title='test'))
        for i in range(3):
            UserData.objects.create(user_id=self.client.session['_auth_user_id'],
                                    lot=Lot.objects.create(date_app_to='2015-12-09'),
                                    chapter=Chapter.objects.create(title='test'))

        for i in range(3):
            UserData.objects.create(user_id=self.client.session['_auth_user_id'],
                                    lot=Lot.objects.create(date_app_to='2015-11-09'),
                                    chapter=Chapter.objects.create(title='test'))

        response = self.client.get('/auction/data_by_month/2015/12/', {}, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response._container[0].count(b'lot_id'), 6)
