# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('name', models.CharField(unique=True, verbose_name='Name', max_length=255)),
                ('position', models.IntegerField(default=0, verbose_name='Position')),
            ],
            options={
                'ordering': ['position'],
            },
        ),
        migrations.CreateModel(
            name='Forum',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('name', models.CharField(verbose_name='Name', max_length=255)),
                ('position', models.IntegerField(default=0, verbose_name='Position')),
                ('description', models.TextField(verbose_name='Description', blank=True)),
                ('is_closed', models.BooleanField(default=False, verbose_name='Is closed')),
                ('category', models.ForeignKey(to='forums.Category', related_name='forums')),
            ],
            options={
                'ordering': ['position'],
            },
        ),
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('created', models.DateTimeField(verbose_name='Created', auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Updated')),
                ('body', models.TextField(verbose_name='Body')),
            ],
            options={
                'ordering': ['created'],
            },
        ),
        migrations.CreateModel(
            name='Topic',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('name', models.CharField(verbose_name='Name', max_length=255)),
                ('forum', models.ForeignKey(to='forums.Forum', related_name='topics')),
                ('last_post', models.ForeignKey(null=True, blank=True, verbose_name='Last post', related_name='forum_last_post', to='forums.Post')),
            ],
            options={
                'ordering': ['-last_post__created'],
            },
        ),
        migrations.AddField(
            model_name='post',
            name='topic',
            field=models.ForeignKey(to='forums.Topic', related_name='posts'),
        ),
        migrations.AddField(
            model_name='post',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, related_name='forum_posts'),
        ),
    ]
