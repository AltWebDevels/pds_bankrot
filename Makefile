##! make

manage = ../venv/bin/python app/manage.py

# new lines

init:
	$(MAKE) run
	docker-compose run --rm web python manage.py collectstatic --noinput
#	docker-compose run --rm web python manage.py migrate auth
	docker-compose run --rm web python manage.py migrate
	sudo touch deploy/.vault_pass
	cp ~/.ssh/id_rsa deploy/.id_rsa
	cp ~/.ssh/id_rsa.pub deploy/.id_rsa.pub
	docker run -it --rm --privileged=true -v $(shell pwd)/layout:/data -v $(shell pwd)/app/static:/static miguelalvarezi/nodejs-bower-gulp sh -c 'npm install && bower i --allow-root --config.interactive=false && gulp build'
	$(MAKE) fix_perms

run:
	docker-compose up -d
#	docker inspect --format 'http://{{ .NetworkSettings.IPAddress }}:8000' pdsbankrot_web_1

clear_docker:
	docker stop $(shell docker ps -a -q)
	docker rm $(shell docker ps -a -q)
	docker rmi $(shell docker ps -a -q)

fix_perms:
#	sudo chown -R $(shell id -u) ./deploy ./data/public
#	sudo chgrp -R $(shell id -g) ./deploy ./data/public
	sudo chmod -R 777 .
	sudo chmod -R 666 deploy/hosts deploy/.vault_pass

frontend_watch:
	docker run -it --rm --privileged=true -v $(shell pwd)/layout:/data -v $(shell pwd)/app/static:/static miguelalvarezi/nodejs-bower-gulp sh -c 'npm install && bower i --allow-root --config.interactive=false && gulp'

frontend_shell:
	docker run -it --rm --privileged=true -v $(shell pwd)/layout:/data miguelalvarezi/nodejs-bower-gulp bash

manage:
	docker-compose run --rm web python manage.py $(COMMAND)

ansible_shell:
	docker run --rm -ti -v $(shell pwd)/deploy:/data pds_bankrot/deploy bash

test_deploy:
	tar -cf ./deploy/.deploy.tar app
	vagrant up
	docker build -t pds_bankrot/deploy ./deploy
	docker run --rm -ti -v $(shell pwd)/deploy:/data pds_bankrot/deploy ansible-playbook server.yml -l local_test --tags="deploy"
	rm ./deploy/.deploy.tar

.PHONY: deploy
deploy:
	tar -cf ./deploy/.deploy.tar app
	docker build -t pds_bankrot/deploy ./deploy
	docker run --rm -ti -v $(shell pwd)/deploy:/data pds_bankrot/deploy ansible-playbook server.yml -l banklot.ru --tags="deploy"
	rm ./deploy/.deploy.tar

shell:
	docker-compose run --rm web bash

load_data:
	$(MAKE) manage COMMAND='loaddata demo_data'

test:
	$(MAKE) manage COMMAND='test'

backup:
	docker build -t pds_bankrot/deploy ./deploy
	docker run --rm -ti -v $(shell pwd)/deploy:/data pds_bankrot/deploy ansible-playbook server.yml -l banklot.ru --tags="backup"

rollback:
	docker build -t pds_bankrot/deploy ./deploy
	docker run --rm -ti -v $(shell pwd)/deploy:/data pds_bankrot/deploy ansible-playbook server.yml -l banklot.ru --tags="rollback"

test_backup:
	vagrant up
	docker build -t pds_bankrot/deploy ./deploy
	docker run --rm -ti -v $(shell pwd)/deploy:/data pds_bankrot/deploy ansible-playbook server.yml -l local_test --tags="backup"

test_rollback:
	vagrant up
	docker build -t pds_bankrot/deploy ./deploy
	docker run --rm -ti -v $(shell pwd)/deploy:/data pds_bankrot/deploy ansible-playbook server.yml -l local_test --tags="rollback"
